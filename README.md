# Jamblox

A recreation of [Minecraft](https://www.minecraft.net/en-us), utilising the Vulkan API as a pose to traditional OpenGL, allowing for more graphics optimisation. Jamblox uses the Corax API for graphics rendering abstraction.

## Disclaimer

Note that this is a educational recreation of Minecraft, which is not currently indended for profit, nor do I intend on violating any of Minecraft's copyright restrictions.
Also note that I am currently using the official original Minecraft texture pack.