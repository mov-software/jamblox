#version 450

#define ATLAS_PIXEL_WIDTH 256
#define ATLAS_PIXEL_HEIGHT 256

layout(binding = 1) uniform sampler2D uTexture;

layout(binding = 2) uniform AmbientLighting
{
    vec4 color;
} ambientLighting;
layout(binding = 3) uniform DiffuseLighting
{
    vec3 direction;
    vec4 color;
} diffuseLighting;

layout(location = 0) in vec3 vNormal;
layout(location = 1) in vec2 vTexel;

layout(location = 0) out vec4 oColor;

vec2 texelToNormalized(vec2 texel)
{
    return texel / 256.0;
}

void main()
{
    const vec3 ambient = ambientLighting.color.xyz * ambientLighting.color.w;

    const vec3 normalDirection = normalize(vNormal);

    vec3 diffuse = max(dot(normalDirection, -diffuseLighting.direction), 0.0) * diffuseLighting.color.xyz * diffuseLighting.color.w;

    oColor = vec4(ambient + diffuse, 1.0) * texture(uTexture, texelToNormalized(vTexel));
}
