#version 450

layout(location = 0) in ivec3 iPosition;
layout(location = 1) in uint iFaces;
layout(location = 2) in uint iBlock;

layout(location = 0) out Block
{
    vec3 position;
    uint faces;
    uint block;
} block;

void main()
{
    block.position = vec3(iPosition);
    block.faces = iFaces;
    block.block = iBlock;
}
