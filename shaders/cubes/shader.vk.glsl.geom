#version 450

layout(binding = 0) uniform Mvp
{
    mat4 mvp;
};

layout(points) in;

layout(location = 0) in Block
{
    vec3 position;
    uint faces;
    uint block;
} blocks[];

layout(triangle_strip, max_vertices = 36) out;

layout(location = 0) out vec3 vNormal;
layout(location = 1) out vec2 vTexel;

#define BIT(i) (1u << i)

#define FRONT_FACE BIT(0)
#define TOP_FACE BIT(1)
#define RIGHT_FACE BIT(2)
#define BACK_FACE BIT(3)
#define BOTTOM_FACE BIT(4)
#define LEFT_FACE BIT(5)

#define ATLAS_PIXEL_WIDTH 256
#define ATLAS_PIXEL_HEIGHT 256

#define TILE_IMAGE_WIDTH 16
#define TILE_IMAGE_HEIGHT 16
#define TILE_PADDING_WIDTH 16
#define TILE_PADDING_HEIGHT 16
#define TILE_WIDTH (TILE_IMAGE_WIDTH+TILE_PADDING_WIDTH)
#define TILE_HEIGHT (TILE_IMAGE_HEIGHT+TILE_PADDING_HEIGHT)

#define ATLAS_TILE_WIDTH (ATLAS_PIXEL_WIDTH/TILE_WIDTH)
#define ATLAS_TILE_HEIGHT (ATLAS_PIXEL_HEIGHT/TILE_HEIGHT)

void outputRelativeVertex(vec3 position, vec3 offset, vec3 normal, vec2 texel)
{
    gl_Position = mvp * vec4(position + offset, 1.0);
    vNormal = normal;
    vTexel = texel;
    EmitVertex();
}

void main()
{
    const vec3 position = blocks[0].position;
    const uint faces = blocks[0].faces;
    const uint block = blocks[0].block;

    const vec2 location = vec2(block % ATLAS_TILE_WIDTH, block / ATLAS_TILE_HEIGHT);
    const vec2 tileToTexel = vec2(TILE_WIDTH, TILE_HEIGHT);

    const vec2 padding = vec2(TILE_IMAGE_WIDTH/2 + 1.0, TILE_IMAGE_HEIGHT/2 + 1.0);

    const mat4x2 tile = mat4x2
        ( (location + vec2(0.0, 0.0)) * tileToTexel + vec2(+padding.x, +padding.y)
        , (location + vec2(0.0, 1.0)) * tileToTexel + vec2(+padding.x, -padding.y)
        , (location + vec2(1.0, 1.0)) * tileToTexel + vec2(-padding.x, -padding.y)
        , (location + vec2(1.0, 0.0)) * tileToTexel + vec2(-padding.x, +padding.y)
    );

    if((faces & FRONT_FACE) != 0)
    {
        outputRelativeVertex(position, vec3(-0.5, -0.5, -0.5), vec3( 0.0,  0.0, -1.0), tile[0]);
        outputRelativeVertex(position, vec3(-0.5, +0.5, -0.5), vec3( 0.0,  0.0, -1.0), tile[1]);
        outputRelativeVertex(position, vec3(+0.5, +0.5, -0.5), vec3( 0.0,  0.0, -1.0), tile[2]);
        EndPrimitive();

        outputRelativeVertex(position, vec3(-0.5, -0.5, -0.5), vec3( 0.0, 0.0, -1.0), tile[0]);
        outputRelativeVertex(position, vec3(+0.5, +0.5, -0.5), vec3( 0.0, 0.0, -1.0), tile[2]);
        outputRelativeVertex(position, vec3(+0.5, -0.5, -0.5), vec3( 0.0, 0.0, -1.0), tile[3]);
        EndPrimitive();
    }

    if((faces & TOP_FACE) != 0)
    {
        outputRelativeVertex(position, vec3(-0.5, +0.5, -0.5), vec3( 0.0, +1.0, 0.0), tile[0]);
        outputRelativeVertex(position, vec3(-0.5, +0.5, +0.5), vec3( 0.0, +1.0, 0.0), tile[1]);
        outputRelativeVertex(position, vec3(+0.5, +0.5, +0.5), vec3( 0.0, +1.0, 0.0), tile[2]);
        EndPrimitive();

        outputRelativeVertex(position, vec3(-0.5, +0.5, -0.5), vec3( 0.0, +1.0, 0.0), tile[0]);
        outputRelativeVertex(position, vec3(+0.5, +0.5, +0.5), vec3( 0.0, +1.0, 0.0), tile[2]);
        outputRelativeVertex(position, vec3(+0.5, +0.5, -0.5), vec3( 0.0, +1.0, 0.0), tile[3]);
        EndPrimitive();
    }

    if((faces & RIGHT_FACE) != 0)
    {
        outputRelativeVertex(position, vec3(+0.5, -0.5, -0.5), vec3(+1.0, 0.0, 0.0), tile[0]);
        outputRelativeVertex(position, vec3(+0.5, +0.5, -0.5), vec3(+1.0, 0.0, 0.0), tile[1]);
        outputRelativeVertex(position, vec3(+0.5, +0.5, +0.5), vec3(+1.0, 0.0, 0.0), tile[2]);
        EndPrimitive();

        outputRelativeVertex(position, vec3(+0.5, -0.5, -0.5), vec3(+1.0, 0.0, 0.0), tile[0]);
        outputRelativeVertex(position, vec3(+0.5, +0.5, +0.5), vec3(+1.0, 0.0, 0.0), tile[2]);
        outputRelativeVertex(position, vec3(+0.5, -0.5, +0.5), vec3(+1.0, 0.0, 0.0), tile[3]);
        EndPrimitive();
    }

    if((faces & BACK_FACE) != 0)
    {
        outputRelativeVertex(position, vec3(+0.5, -0.5, +0.5), vec3( 0.0, 0.0, +1.0), tile[0]);
        outputRelativeVertex(position, vec3(+0.5, +0.5, +0.5), vec3( 0.0, 0.0, +1.0), tile[1]);
        outputRelativeVertex(position, vec3(-0.5, +0.5, +0.5), vec3( 0.0, 0.0, +1.0), tile[2]);
        EndPrimitive();

        outputRelativeVertex(position, vec3(+0.5, -0.5, +0.5), vec3( 0.0, 0.0, +1.0), tile[0]);
        outputRelativeVertex(position, vec3(-0.5, +0.5, +0.5), vec3( 0.0, 0.0, +1.0), tile[2]);
        outputRelativeVertex(position, vec3(-0.5, -0.5, +0.5), vec3( 0.0, 0.0, +1.0), tile[3]);
        EndPrimitive();
    }

    if((faces & BOTTOM_FACE) != 0)
    {
        outputRelativeVertex(position, vec3(-0.5, -0.5, +0.5), vec3( 0.0, -1.0, 0.0), tile[0]);
        outputRelativeVertex(position, vec3(-0.5, -0.5, -0.5), vec3( 0.0, -1.0, 0.0), tile[1]);
        outputRelativeVertex(position, vec3(+0.5, -0.5, -0.5), vec3( 0.0, -1.0, 0.0), tile[2]);
        EndPrimitive();

        outputRelativeVertex(position, vec3(-0.5, -0.5, +0.5), vec3( 0.0, -1.0, 0.0), tile[0]);
        outputRelativeVertex(position, vec3(+0.5, -0.5, -0.5), vec3( 0.0, -1.0, 0.0), tile[2]);
        outputRelativeVertex(position, vec3(+0.5, -0.5, +0.5), vec3( 0.0, -1.0, 0.0), tile[3]);
        EndPrimitive();
    }

    if((faces & LEFT_FACE) != 0)
    {
        outputRelativeVertex(position, vec3(-0.5, -0.5, +0.5), vec3(-1.0, 0.0, 0.0), tile[0]);
        outputRelativeVertex(position, vec3(-0.5, +0.5, +0.5), vec3(-1.0, 0.0, 0.0), tile[1]);
        outputRelativeVertex(position, vec3(-0.5, +0.5, -0.5), vec3(-1.0, 0.0, 0.0), tile[2]);
        EndPrimitive();

        outputRelativeVertex(position, vec3(-0.5, -0.5, +0.5), vec3(-1.0, 0.0, 0.0), tile[0]);
        outputRelativeVertex(position, vec3(-0.5, +0.5, -0.5), vec3(-1.0, 0.0, 0.0), tile[2]);
        outputRelativeVertex(position, vec3(-0.5, -0.5, -0.5), vec3(-1.0, 0.0, 0.0), tile[3]);
        EndPrimitive();
    }
}
