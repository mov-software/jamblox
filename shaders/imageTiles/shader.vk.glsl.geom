#version 450

layout(points) in;

layout(location = 0) in Tile
{
    vec2 centre;
    float radius;
    uint block;
} tiles[];

layout(triangle_strip, max_vertices = 6) out;

layout(location = 0) out vec2 vTexel;

#define ATLAS_PIXEL_WIDTH 256
#define ATLAS_PIXEL_HEIGHT 256

#define TILE_IMAGE_WIDTH 16
#define TILE_IMAGE_HEIGHT 16
#define TILE_PADDING_WIDTH 16
#define TILE_PADDING_HEIGHT 16
#define TILE_WIDTH (TILE_IMAGE_WIDTH+TILE_PADDING_WIDTH)
#define TILE_HEIGHT (TILE_IMAGE_HEIGHT+TILE_PADDING_HEIGHT)

#define ATLAS_TILE_WIDTH (ATLAS_PIXEL_WIDTH/TILE_WIDTH)
#define ATLAS_TILE_HEIGHT (ATLAS_PIXEL_HEIGHT/TILE_HEIGHT)

void outputVertex(vec2 position, vec2 texel)
{
    gl_Position = vec4(position, 0.0, 1.0);
    vTexel = texel;
    EmitVertex();
}

void main()
{
    const vec2 centre = tiles[0].centre;
    const float radius = tiles[0].radius;
    const uint block = tiles[0].block;

    const vec2 location = vec2(block % ATLAS_TILE_WIDTH, block / ATLAS_TILE_HEIGHT);
    const vec2 tileToTexel = vec2(TILE_WIDTH, TILE_HEIGHT);

    const vec2 padding = vec2(TILE_IMAGE_WIDTH/2 + 1.0, TILE_IMAGE_HEIGHT/2 + 1.0);

    const mat4x2 tile = mat4x2
        ( (location + vec2(0.0, 0.0)) * tileToTexel + vec2(+padding.x, +padding.y)
        , (location + vec2(0.0, 1.0)) * tileToTexel + vec2(+padding.x, -padding.y)
        , (location + vec2(1.0, 1.0)) * tileToTexel + vec2(-padding.x, -padding.y)
        , (location + vec2(1.0, 0.0)) * tileToTexel + vec2(-padding.x, +padding.y)
    );
    const mat4x2 corners = mat4x2
        ( centre + vec2(-radius, -radius)
        , centre + vec2(-radius, +radius)
        , centre + vec2(+radius, +radius)
        , centre + vec2(+radius, -radius)
    );

    outputVertex(corners[0], tile[0]);
    outputVertex(corners[1], tile[1]);
    outputVertex(corners[2], tile[2]);
    EndPrimitive();

    outputVertex(corners[0], tile[0]);
    outputVertex(corners[2], tile[2]);
    outputVertex(corners[3], tile[3]);
    EndPrimitive();
}
