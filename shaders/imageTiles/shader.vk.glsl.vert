#version 450

layout(location = 0) in vec2 iCentre;
layout(location = 1) in float iRadius;
layout(location = 2) in uint iBlock;

layout(location = 0) out Tile
{
    vec2 centre;
    float radius;
    uint block;
} tile;

void main()
{
    tile.centre = vec2(iCentre.x, -iCentre.y);
    tile.radius = iRadius;
    tile.block = iBlock;
}
