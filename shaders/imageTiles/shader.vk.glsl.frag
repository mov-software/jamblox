#version 450

#define ATLAS_PIXEL_WIDTH 256
#define ATLAS_PIXEL_HEIGHT 256

layout(binding = 0) uniform sampler2D uTexture;

layout(location = 0) in vec2 vTexel;

layout(location = 0) out vec4 oColor;

vec2 texelToNormalized(vec2 texel)
{
    return texel / 256.0;
}

void main()
{
    oColor = texture(uTexture, texelToNormalized(vTexel));
}
