#version 450

layout(binding = 0) uniform Color
{
    vec4 color;
};

layout(location = 0) out vec4 oColor;

void main()
{
    oColor = color;
}
