set ( SHADER_VK_GLSL_VERT_FILES

    "${JAMBLOX_SHADERS_DIR}/cubes/shader.vk.glsl.vert"
    "${JAMBLOX_SHADERS_DIR}/lines/shader.vk.glsl.vert"
    "${JAMBLOX_SHADERS_DIR}/imageTiles/shader.vk.glsl.vert"
)
set ( SHADER_VK_GLSL_FRAG_FILES

    "${JAMBLOX_SHADERS_DIR}/cubes/shader.vk.glsl.frag"
    "${JAMBLOX_SHADERS_DIR}/lines/shader.vk.glsl.frag"
    "${JAMBLOX_SHADERS_DIR}/imageTiles/shader.vk.glsl.frag"
)
set ( SHADER_VK_GLSL_GEOM_FILES

    "${JAMBLOX_SHADERS_DIR}/cubes/shader.vk.glsl.geom"
    "${JAMBLOX_SHADERS_DIR}/imageTiles/shader.vk.glsl.geom"
)

string(REGEX REPLACE ".vk.glsl.vert" ".vert.spv;" SHADER_VERT_SPV_FILES ${SHADER_VK_GLSL_VERT_FILES})
string(REGEX REPLACE ".vk.glsl.frag" ".frag.spv;" SHADER_FRAG_SPV_FILES ${SHADER_VK_GLSL_FRAG_FILES})
string(REGEX REPLACE ".vk.glsl.geom" ".geom.spv;" SHADER_GEOM_SPV_FILES ${SHADER_VK_GLSL_GEOM_FILES})

# Retrieving the available GLSL compiler
execute_process(COMMAND ${CX_FIND_GLSLC_SH_SCRIPT} OUTPUT_VARIABLE GLSLC)

if(NOT GLSLC)
    message(FATAL_ERROR "GLSL compiler not found")
endif()

# Compiling GLSL vertex shader source code into SPIR-V bytecode
foreach(SHADER_VK_GLSL_VERT_FILE ${SHADER_VK_GLSL_VERT_FILES})
    string(REGEX REPLACE ".vk.glsl.vert" ".vert.spv" SHADER_VERT_SPV_FILE ${SHADER_VK_GLSL_VERT_FILE})

    add_custom_command ( OUTPUT ${SHADER_VERT_SPV_FILE}

        COMMAND ${GLSLC} ${SHADER_VK_GLSL_VERT_FILE} -o ${SHADER_VERT_SPV_FILE}
        DEPENDS ${SHADER_VK_GLSL_VERT_FILE}
    )
endforeach()
# Compiling GLSL fragment shader source code into SPIR-V bytecode
foreach(SHADER_VK_GLSL_FRAG_FILE ${SHADER_VK_GLSL_FRAG_FILES})
    string(REGEX REPLACE ".vk.glsl.frag" ".frag.spv" SHADER_FRAG_SPV_FILE ${SHADER_VK_GLSL_FRAG_FILE})

    add_custom_command ( OUTPUT ${SHADER_FRAG_SPV_FILE}

        COMMAND ${GLSLC} ${SHADER_VK_GLSL_FRAG_FILE} -o ${SHADER_FRAG_SPV_FILE}
        DEPENDS ${SHADER_VK_GLSL_FRAG_FILE}
    )
endforeach()
# Compiling GLSL geometry shader source code into SPIR-V bytecode
foreach(SHADER_VK_GLSL_GEOM_FILE ${SHADER_VK_GLSL_GEOM_FILES})
    string(REGEX REPLACE ".vk.glsl.geom" ".geom.spv" SHADER_GEOM_SPV_FILE ${SHADER_VK_GLSL_GEOM_FILE})

    add_custom_command ( OUTPUT ${SHADER_GEOM_SPV_FILE}

        COMMAND ${GLSLC} ${SHADER_VK_GLSL_GEOM_FILE} -o ${SHADER_GEOM_SPV_FILE}
        DEPENDS ${SHADER_VK_GLSL_GEOM_FILE}
    )
endforeach()

add_custom_target ( jamblox-shaders

    DEPENDS ${SHADER_VERT_SPV_FILES}
    DEPENDS ${SHADER_FRAG_SPV_FILES}
    DEPENDS ${SHADER_GEOM_SPV_FILES}
)
