// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

#include <jamblox/graphics/renderer.hh>
#include <jamblox/graphics/layers/linesOverlay.hh>
#include <jamblox/graphics/layers/imageTilesOverlay.hh>
#include <jamblox/graphics/layers/gui.hh>

#include <jamblox/world/superflat.hh>
#include <jamblox/world/natural.hh>

#include <unordered_set>

using cx::decimal;
using cx::Euler_D;
using cx::Vector2d;
using cx::Vector2D;
using cx::Vector3D;
using cx::Vector4D;
using cx::Matrix4x4D;

class Jamblox final
    : public cx::io::glfw::Application
    , public cx::rhi::vk::Application
{
private:
    std::shared_ptr<cx::io::glfw::Window> window;

    cx::Tgr<std::shared_ptr<cx::rhi::vk::PhysicalDevice>> physicalDevice;

    cx::Tgr<std::vector<cx::rhi::vk::QueueFamily>> queueFamilies;
    cx::Tgr<cx::rhi::vk::QueueFamily> transferQueueFamily;
    cx::Tgr<cx::rhi::vk::QueueFamily> graphicsQueueFamily;
    cx::Tgr<cx::rhi::vk::QueueFamily> presentQueueFamily;

    cx::Tgr<std::shared_ptr<cx::rhi::vk::Surface>> surface;
    cx::Tgr<VkSurfaceFormatKHR> surfaceFormat;
    cx::Tgr<VkPresentModeKHR> presentMode;

    cx::Tgr<VkExtent2D> resolution;

    cx::Tgr<VkSampleCountFlagBits> samples;

    cx::Tgr<std::unique_ptr<Renderer>> renderer;

    cx::Tgr<std::shared_ptr<Viewport>> viewport;
    cx::Tgr<std::shared_ptr<LinesOverlay>> linesOverlay;
    cx::Tgr<std::shared_ptr<ImageTilesOverlay>> imageTilesOverlay;
    cx::Tgr<std::shared_ptr<Gui>> gui;

    cx::Tgr<std::unique_ptr<NaturalWorld>> world;

    std::unordered_set<ChunkLoc> currentChunkLocs;

    cx::Tgr<cx::Bitmap> textureAtlas;

    std::unordered_map<cx::io::glfw::KeyCode, bool> buttons;

    Player player;

    Lighting lighting;

    Settings settings;
    Statistics statistics;

    bool menu = false;

    cx::decimal
        inventoryTileSize = 0.25_D,
        inventoryTilePadding = 0.04_D,
        inventoryVerticalPosition = -0.75_D;
public:
	explicit Jamblox(cx::Application::Mode mode);

    ~Jamblox() override;

	void main() override;
private:
    void update();
    void updateMovement(decimal t);

    void selectPhysicalDevice(const std::vector<std::shared_ptr<cx::rhi::vk::PhysicalDevice>>& physicalDevices);
    void getQueueFamilies();
    void getTransferQueueFamily();
    void getGraphicsQueueFamily();
    void getPresentQueueFamily();

    void createWindow();
    void createSurface();
    void getSurfaceFormat();
    void getPresentMode();
    void getResolution();
    void getSamples();
    void createRenderer();
    void createViewport();
    void createLinesOverlay();
    void createImageTilesOverlay();
    void createGui();
    void addLayers();
    void loadTextureAtlas(const std::string& filePath);
    void loadWorld();
    void loadTexture(cx::Ref<cx::rhi::vk::SubmissionQueue> submissionQueue);
    void loadDescriptorSets();
    void loadInventoryOverlayData(cx::Ref<cx::rhi::vk::SubmissionQueue> submissionQueue);
    void loadResources();
    void addWindowEventListeners();
    void toggleMenu();

    std::unordered_set<ChunkLoc> getVisibleChunkLocs();
    void updateVisibleChunks();

    void changeBlock(const Coordinate& coordinate, Block block);
};

void toLower(char* ptr);

cx::Application::Mode getApplicationMode(int argc, char** argv);
