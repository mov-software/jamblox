// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

#include "inventory.hh"

Inventory::Inventory(std::array<Block, size> blocks, unsigned int index)
    : blocks(blocks)
    , index(index)
{}

Block& Inventory::operator[](unsigned int index)
{
    return blocks[index];
}
const Block& Inventory::operator[](unsigned int index) const
{
    return blocks[index];
}

std::array<Block, Inventory::size>::iterator Inventory::begin()
{
    return blocks.begin();
}
std::array<Block, Inventory::size>::iterator Inventory::end()
{
    return blocks.end();
}
std::array<Block, Inventory::size>::const_iterator Inventory::cbegin() const
{
    return blocks.cbegin();
}
std::array<Block, Inventory::size>::const_iterator Inventory::cend() const
{
    return blocks.cend();
}

Block Inventory::getSelected() const
{
    return blocks[index];
}
