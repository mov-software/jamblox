// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

#ifndef JAMBLOX_ENTITY_INVENTORY_HH
#define JAMBLOX_ENTITY_INVENTORY_HH

#include <jamblox/world/block.hh>

#include <array>

class Inventory
{
public:
    constexpr static unsigned int size = 4u;

    std::array<Block, size> blocks;
    unsigned int index;

    Inventory(std::array<Block, size> blocks, unsigned int index = 0u);

    Block& operator[](unsigned int index);
    const Block& operator[](unsigned int index) const;

    std::array<Block, size>::iterator begin();
    std::array<Block, size>::iterator end();
    std::array<Block, size>::const_iterator cbegin() const;
    std::array<Block, size>::const_iterator cend() const;

    Block getSelected() const;
};

#endif // JAMBLOX_ENTITY_INVENTORY_HH
