// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

#include <jamblox/jamblox.hh>

#include <cstring>

void toLower(char* ptr)
{
    for(; *ptr; ++ptr)
    {
        *ptr = tolower(*ptr);
    }
}

cx::Application::Mode getApplicationMode(int argc, char** argv)
{
    cx::Application::Mode mode = cx::RELEASE;

    if(argc > 1)
    {
        char* arg = argv[1];
        toLower(arg);

        if(strcmp(arg, "debug") == 0 || strcmp(arg, "d") == 0)
        {
            mode = cx::DEBUG;
        }
    }

    return mode;
}

int main(int argc, char** argv)
{
    cx::Application::run<Jamblox>(getApplicationMode(argc, argv));
}
