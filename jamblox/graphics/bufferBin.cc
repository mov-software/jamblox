// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

#include "bufferBin.hh"

BufferBin::Node::Node(std::shared_ptr<cx::rhi::vk::Buffer> buffer)
    : buffer(std::move(buffer))
{}

BufferBin::BufferBin(uint32_t frameCount)
    : frameCount(frameCount)
{}

void BufferBin::add(std::shared_ptr<cx::rhi::vk::Buffer> buffer)
{
    nodes.emplace_back(buffer);
}
void BufferBin::clear(uint32_t frameIndex)
{
    for(std::vector<Node>::iterator node = nodes.begin(); node < nodes.end(); ++node)
    {
        node->clearedFrameIndices.insert(frameIndex);

        if(node->clearedFrameIndices.size() >= frameCount)
        {
            nodes.erase(node--);
        }
    }
}
void BufferBin::clear()
{
    nodes.clear();
}
