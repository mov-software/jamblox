// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

#ifndef JAMBLOX_GRAPHICS_RENDERER_HH
#define JAMBLOX_GRAPHICS_RENDERER_HH

#include <jamblox/world/space.hh>

#include <jamblox/graphics/bufferBin.hh>

#include <corax/corax.hh>

class Renderer : public cx::Renderer
{
public:
    const VkSampleCountFlagBits samples;

    cx::Tgr<std::shared_ptr<cx::rhi::vk::RenderPass>> renderPass;

    BufferBin bufferBin;
private:
    cx::Tgr<std::shared_ptr<cx::rhi::vk::Attachment>> colorAttachment;
    cx::Tgr<std::shared_ptr<cx::rhi::vk::Attachment>> depthAttachment;

    cx::Tgr<std::vector<std::shared_ptr<cx::rhi::vk::Framebuffer>>> framebuffers;
public:
    Renderer
        ( std::shared_ptr<cx::rhi::vk::PhysicalDevice> physicalDevice
        , std::vector<cx::rhi::vk::QueueFamily> queueFamilies
        , cx::rhi::vk::QueueFamily transferQueueFamily
        , cx::rhi::vk::QueueFamily graphicsQueueFamily
        , cx::rhi::vk::QueueFamily presentQueueFamily
        , std::shared_ptr<cx::rhi::vk::Surface> surface
        , VkSurfaceFormatKHR surfaceFormat
        , VkPresentModeKHR presentMode
        , VkExtent2D resolution
        , VkSampleCountFlagBits samples
    );

    ~Renderer();
private:
    void recreateSwapchainResources(VkExtent2D extent) override;

    void createRenderPass();
    void createColorAttachment();
    void createDepthAttachment();
    void createFramebuffers();
public:
    cx::rhi::vk::SubmissionQueue* createTransferSubmissionQueue(bool transient = true, bool reused = false);

    void recordDraw(unsigned int i);
    void draw(const std::function<VkExtent2D()>& getExtent);
};

#endif // JAMBLOX_GRAPHICS_RENDERER_HH
