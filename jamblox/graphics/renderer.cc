// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

#include "renderer.hh"

#include <jamblox/world/chunk.hh>

Renderer::Renderer
    ( std::shared_ptr<cx::rhi::vk::PhysicalDevice> physicalDevice
    , std::vector<cx::rhi::vk::QueueFamily> queueFamilies
    , cx::rhi::vk::QueueFamily transferQueueFamily
    , cx::rhi::vk::QueueFamily graphicsQueueFamily
    , cx::rhi::vk::QueueFamily presentQueueFamily
    , std::shared_ptr<cx::rhi::vk::Surface> surface
    , VkSurfaceFormatKHR surfaceFormat
    , VkPresentModeKHR presentMode
    , VkExtent2D resolution
    , VkSampleCountFlagBits samples
)
    : cx::Renderer
        ( std::move(physicalDevice)
        , queueFamilies
        , transferQueueFamily
        , graphicsQueueFamily
        , presentQueueFamily
        , std::move(surface)
        , surfaceFormat
        , presentMode
        , resolution
    )
    , samples(samples)
    , bufferBin(swapchain.retrieve()->imageCount())
{
    cx::Workload workload;

    workload.add(&Renderer::createRenderPass, this);
    workload.add(&Renderer::createColorAttachment, this);
    workload.add(&Renderer::createDepthAttachment, this);
    workload.add(&Renderer::createFramebuffers, this);
}

Renderer::~Renderer()
{
    device.retrieve()->waitIdle();

    bufferBin.clear();
}

void Renderer::recreateSwapchainResources(VkExtent2D extent)
{
    cx::Renderer::recreateSwapchainResources(extent);

    renderPass.reset();
    colorAttachment.reset();
    depthAttachment.reset();
    framebuffers.reset();

    device.retrieve()->waitIdle();

    cx::Workload workload;

    workload.add(&Renderer::createRenderPass, this);
    workload.add(&Renderer::createColorAttachment, this);
    workload.add(&Renderer::createDepthAttachment, this);
    workload.add(&Renderer::createFramebuffers, this);
    workload.wait();

    for(const std::shared_ptr<cx::Layer>& layer : layers)
    {
        layer->recreateSwapchainResources();
    }
}
void Renderer::createRenderPass()
{
    unsigned int
        attachmentCount = 0u,
        colorAttachmentIndex = 0u,
        depthAttachmentIndex = 0u,
        resolveAttachmentIndex = 0u;

    colorAttachmentIndex = attachmentCount++;
    depthAttachmentIndex = attachmentCount++;

    if(samples != VK_SAMPLE_COUNT_1_BIT)
    {
        resolveAttachmentIndex = attachmentCount++;
    }

    std::vector<VkAttachmentDescription> attachments(attachmentCount);
    std::vector<VkAttachmentReference> attachmentReferences(attachmentCount);

    attachments[colorAttachmentIndex] =
    {
        .flags = 0,
        .format = surfaceFormat.format,
        .samples = samples,
        .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
        .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
        .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
    };
    attachmentReferences[colorAttachmentIndex] =
    {
        .attachment = static_cast<uint32_t>(colorAttachmentIndex),
        .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
    };

    attachments[depthAttachmentIndex] =
    {
        .flags = 0,
        .format = VK_FORMAT_D32_SFLOAT,
        .samples = samples,
        .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
        .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
        .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
    };
    attachmentReferences[depthAttachmentIndex] =
    {
        .attachment = static_cast<uint32_t>(depthAttachmentIndex),
        .layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
    };

    if(samples != VK_SAMPLE_COUNT_1_BIT)
    {
        attachments[resolveAttachmentIndex] =
        {
            .flags = 0,
            .format = surfaceFormat.format,
            .samples = VK_SAMPLE_COUNT_1_BIT,
            .loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
            .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
            .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
            .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
            .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
            .finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
        };
        attachmentReferences[resolveAttachmentIndex] =
        {
            .attachment = static_cast<uint32_t>(resolveAttachmentIndex),
            .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
        };
    }

    const std::vector<VkSubpassDescription> subPasses
    {
        {
            .flags = 0,
            .pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
            .inputAttachmentCount = 0,
            .pInputAttachments = nullptr,
            .colorAttachmentCount = 1,
            .pColorAttachments = &attachmentReferences[colorAttachmentIndex],
            .pResolveAttachments = &attachmentReferences[resolveAttachmentIndex],
            .pDepthStencilAttachment = &attachmentReferences[depthAttachmentIndex],
            .preserveAttachmentCount = 0,
            .pPreserveAttachments = nullptr,
        }
    };
    const std::vector<VkSubpassDependency> dependencies
    {
        {
            .srcSubpass = VK_SUBPASS_EXTERNAL,
            .dstSubpass = 0,
            .srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
            .dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
            .srcAccessMask = 0,
            .dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
            .dependencyFlags = 0,
        }
    };

    renderPass.initialize
        ( std::make_shared<cx::rhi::vk::RenderPass>
            ( device.retrieve()
            , attachments
            , subPasses
            , dependencies
        )
    );
}

void Renderer::createColorAttachment()
{
    colorAttachment.initialize
        ( std::make_shared<cx::rhi::vk::Attachment>
            ( std::make_shared<cx::rhi::vk::CustomImage>
                ( device.retrieve()
                , std::vector<uint32_t>
                {
                    graphicsQueue.retrieve()->family.index,
                }
                , VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT
                , surfaceFormat.format
                , VK_IMAGE_LAYOUT_UNDEFINED
                , VkExtent3D
                {
                    resolution.width,
                    resolution.height,
                    1,
                }
                , VK_IMAGE_TILING_OPTIMAL
                , VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
                , 1
                , samples
            ),
            VK_IMAGE_ASPECT_COLOR_BIT
        )
    );
}
void Renderer::createDepthAttachment()
{
    depthAttachment.initialize
        ( std::make_shared<cx::rhi::vk::Attachment>
            ( std::make_shared<cx::rhi::vk::CustomImage>
                ( device.retrieve()
                , std::vector<uint32_t>
                {
                    graphicsQueue.retrieve()->family.index
                }
                , VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT
                , VK_FORMAT_D32_SFLOAT
                , VK_IMAGE_LAYOUT_UNDEFINED
                , VkExtent3D
                {
                    resolution.width,
                    resolution.height,
                    1,
                }
                , VK_IMAGE_TILING_OPTIMAL
                , VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
                , 1
                , samples
            ),
            VK_IMAGE_ASPECT_DEPTH_BIT
        )
    );
}
void Renderer::createFramebuffers()
{
    std::vector<std::shared_ptr<cx::rhi::vk::Framebuffer>> framebuffersLocal(swapchain.retrieve()->imageCount());

    for(unsigned int i = 0u; i < swapchain.retrieve()->imageCount(); ++i)
    {
        const std::vector<cx::Ref<cx::rhi::vk::ImageView>> attachments
        {
            colorAttachment.retrieve()->imageView,
            depthAttachment.retrieve()->imageView,
            swapchain.retrieve()->imageView(i),
        };

        framebuffersLocal[i] = std::make_shared<cx::rhi::vk::Framebuffer>
            ( device.retrieve()
            , renderPass.retrieve()
            , attachments
            , resolution
        );
    }

    framebuffers.initialize(framebuffersLocal);
}

cx::rhi::vk::SubmissionQueue* Renderer::createTransferSubmissionQueue(bool transient, bool reused)
{
    return new cx::rhi::vk::SubmissionQueue
        ( transferQueue.retrieve()
        , transient
        , reused
    );
}

void Renderer::recordDraw(unsigned int i)
{
    cx::TgrWriteLock renderCommandBuffersLock(primaryCommandBuffers);
    std::vector<cx::rhi::vk::CommandBuffer>& renderCommandBuffers = this->primaryCommandBuffers.retrieve(renderCommandBuffersLock);

    cx::rhi::vk::CommandBuffer cmd = renderCommandBuffers[i];

    std::vector<VkClearValue> clearValues(2);
    clearValues[0].color = { 0.47f, 0.67f, 1.0f, 1.0f };
    clearValues[1].depthStencil = { 1.0f, 0 };

    cmd.begin(VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT);

    cmd.beginRenderPass(renderPass.retrieve(), framebuffers.retrieve()[i], clearValues, resolution);

    for(const std::shared_ptr<cx::Layer>& layer : layers)
    {
        layer->submitCommands(cmd);
    }

    cmd.endRenderPass();

    cmd.end();
}
void Renderer::draw(const std::function<VkExtent2D()>& getExtent)
{
    std::shared_ptr<cx::rhi::vk::Semaphore>
        imageAvailableSemaphore = imageAvailableSemaphores.retrieve()[frameIndex],
        renderFinishedSemaphore = renderFinishedSemaphores.retrieve()[frameIndex];

    uint32_t imageIndex;
    try
    {
        imageIndex = swapchain.retrieve()->acquireNextImageIndex(imageAvailableSemaphore, nullptr);
    }
    catch(const cx::rhi::vk::SwapchainSuboptimalException&) {}
    catch(const cx::rhi::vk::SwapchainOutOfDateError&)
    {
        recreateSwapchainResources(getExtent());
        return;
    }
    catch(const cx::rhi::vk::Exception& exception)
    {
        cx::rhi::vk::logger.error(exception.what());
    }

    for(const std::shared_ptr<cx::Layer>& layer : layers)
    {
        layer->update(imageIndex);
    }

    const std::shared_ptr<cx::rhi::vk::Fence>& inFlightFence = inFlightFences.retrieve()[imageIndex];
    inFlightFence->wait();
    inFlightFence->reset();

    bufferBin.clear(imageIndex);

    recordDraw(imageIndex);

    graphicsQueue.retrieve()->submit
        ( cx::rhi::vk::Queue::Submission
            (primaryCommandBuffers.retrieve()[imageIndex]
            , std::vector<cx::rhi::vk::WaitStage>
            {
                cx::rhi::vk::WaitStage
                    ( VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT
                    , imageAvailableSemaphore
                )
            }
            , { renderFinishedSemaphore }
        )
        , inFlightFence
    );

    try
    {
        swapchain.retrieve()->present
            ( presentQueue.retrieve()
            , { renderFinishedSemaphore }
            , imageIndex
        );
    }
    catch(const cx::rhi::vk::SwapchainSuboptimalException&)
    {
        recreateSwapchainResources(getExtent());
    }
    catch(const cx::rhi::vk::SwapchainOutOfDateError&)
    {
        recreateSwapchainResources(getExtent());
    }
    catch(const cx::rhi::vk::Exception& exception)
    {
        cx::rhi::vk::logger.error(exception.what());
    }

    frameIndex = (frameIndex + 1) % swapchain.retrieve()->imageCount();
}
