// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

#include "gui.hh"

#include <jamblox/world/chunk.hh>

#include <imgui/backends/imgui_impl_glfw.h>
#include <imgui/backends/imgui_impl_vulkan.h>

Gui::Gui
    ( Renderer& renderer
    , cx::Ref<cx::io::glfw::Window> window
    , cx::Ref<cx::rhi::vk::Instance> instance
    , Settings& settings
    , Statistics& statistics
)
    : Layer(renderer)
    , settings(settings)
    , statistics(statistics)
{
    createDescriptorPool();

    IMGUI_CHECKVERSION();
    ImGui::CreateContext();

    ImGui::StyleColorsDark();

    cx::rhi::vk::Device& device = *this->device.retrieve();
    std::shared_ptr<cx::rhi::vk::Queue> graphicsQueue = this->graphicsQueue.retrieve();

    ImGui_ImplGlfw_InitForVulkan(window->handle, true);
    ImGui_ImplVulkan_InitInfo initInfo
    {
        .Instance = instance->handle,
        .PhysicalDevice = device.physical->handle,
        .Device = device.handle,
        .QueueFamily = graphicsQueue->family.index,
        .Queue = graphicsQueue->handle,
        .PipelineCache = VK_NULL_HANDLE,
        .DescriptorPool = descriptorPool.retrieve()->handle,
        .Subpass = 0,
        .MinImageCount = 2,
        .ImageCount = static_cast<uint32_t>(swapchain.retrieve()->imageCount()),
        .MSAASamples = renderer.samples,
        .Allocator = nullptr,
        .CheckVkResultFn = nullptr,
    };

    ImGui_ImplVulkan_Init(&initInfo, renderer.renderPass.retrieve()->handle);

    std::unique_ptr<cx::rhi::vk::SubmissionQueue> submissionQueue(renderer.createTransferSubmissionQueue());

    cx::rhi::vk::CommandChain& commandChain = submissionQueue->next();
    cx::rhi::vk::CommandBuffer cmd = commandChain.open();

    ImGui_ImplVulkan_CreateFontsTexture(cmd.handle);

    commandChain.close();

    cx::rhi::vk::Fence fence(submissionQueue->queue->device);
    submissionQueue->submit(&fence);

    fence.wait();

    ImGui_ImplVulkan_DestroyFontUploadObjects();
}

Gui::~Gui()
{
    ImGui_ImplVulkan_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();
}

void Gui::showMenu()
{
    menuOpen = true;
}
void Gui::hideMenu()
{
    menuOpen = false;
}

void Gui::toggleStats()
{
    statisticsOpen = !statisticsOpen;
}

void Gui::createDescriptorPool()
{
    const uint32_t maxSize = 1000;
    const std::vector<VkDescriptorPoolSize> poolSizes
    {
        { VK_DESCRIPTOR_TYPE_SAMPLER, maxSize },
        { VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, maxSize },
        { VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, maxSize },
        { VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, maxSize },
        { VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER, maxSize },
        { VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER, maxSize },
        { VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, maxSize },
        { VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, maxSize },
        { VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, maxSize },
        { VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC, maxSize },
        { VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT, maxSize }
    };

    descriptorPool.initialize
        ( std::make_shared<cx::rhi::vk::DescriptorPool>
            ( device.retrieve()
            , maxSize * poolSizes.size()
            , poolSizes
            , true
        )
    );
}

void Gui::recreateSwapchainResources() {}

void Gui::submitCommands(cx::rhi::vk::CommandBuffer primary)
{
    if (drawData)
    {
        ImGui_ImplVulkan_RenderDrawData(drawData, primary.handle);
    }
}
void Gui::update(unsigned int)
{
    ImGui_ImplVulkan_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    if(menuOpen)
    {
        displayMenu();
    }
    if(statisticsOpen)
    {
        displayStatistics();
    }

    ImGui::Render();

    drawData = ImGui::GetDrawData();
}

void Gui::displayMenu()
{
    const ImGuiViewport* viewport = ImGui::GetMainViewport();
    ImGui::SetNextWindowPos(ImVec2(viewport->WorkSize.x / 4, 20), 0);
    ImGui::SetNextWindowSize(ImVec2(viewport->WorkSize.x / 2, viewport->WorkSize.y - 40), 0);

    ImGui::Begin
        ( "Menu"
        , nullptr
        , ImGuiWindowFlags_NoCollapse
        | ImGuiWindowFlags_NoResize
    );

    if(ImGui::BeginTabBar("Menu"))
    {
        displayMenuSettings();
        displayMenuHelp();
        displayMenuInfo();

        ImGui::EndTabBar();
    }

    ImGui::End();
}
void Gui::displayMenuSettings()
{
    if(ImGui::BeginTabItem("Settings"))
    {
        ImGui::SeparatorText("Settings");

        ImGui::SetNextItemOpen(true, ImGuiCond_FirstUseEver);
        if(ImGui::CollapsingHeader("World"))
        {
            cx::TgrWriteLock renderDistanceLock(settings.renderDistance);
            ImGui::SliderInt("Render distance", &settings.renderDistance.retrieve(renderDistanceLock), 1, 10);
            renderDistanceLock.unlock();
        }
        ImGui::SetNextItemOpen(true, ImGuiCond_FirstUseEver);
        if(ImGui::CollapsingHeader("Sensitivity"))
        {
            cx::TgrWriteLock movementSpeedLock(settings.movementSpeed);
            ImGui::SliderFloat("Movement speed", &settings.movementSpeed.retrieve(movementSpeedLock), 1.0f, 10.0f);
            movementSpeedLock.unlock();

            cx::TgrWriteLock rotationSpeedLock(settings.rotationSpeed);
            cx::Vector2D& rotationSpeed = settings.rotationSpeed.retrieve(rotationSpeedLock);
            ImGui::SliderFloat2("Rotation speed", &rotationSpeed.x, 0.1f, 5.0f);
            rotationSpeedLock.unlock();
        }

        ImGui::EndTabItem();
    }
}
void Gui::displayMenuHelp()
{
    if(ImGui::BeginTabItem("Help"))
    {
        ImGui::SeparatorText("Help");

        ImGui::SetNextItemOpen(true, ImGuiCond_FirstUseEver);
        if(ImGui::CollapsingHeader("Controls"))
        {
            ImGui::TextWrapped("Use the mouse for panning the view around, and the WASD keys for movement");
            if(ImGui::BeginTable("Key bindings", 2, ImGuiTableFlags_Borders))
            {
                {
                    ImGui::TableSetupColumn("Key");
                    ImGui::TableSetupColumn("Function");
                    ImGui::TableHeadersRow();
                }
                {
                    ImGui::TableNextColumn(); ImGui::Text("W");
                    ImGui::TableNextColumn(); ImGui::TextWrapped("Move forwards");
                }
                {
                    ImGui::TableNextColumn(); ImGui::Text("A");
                    ImGui::TableNextColumn(); ImGui::TextWrapped("Move left");
                }
                {
                    ImGui::TableNextColumn(); ImGui::Text("S");
                    ImGui::TableNextColumn(); ImGui::TextWrapped("Move backwards");
                }
                {
                    ImGui::TableNextColumn(); ImGui::Text("D");
                    ImGui::TableNextColumn(); ImGui::TextWrapped("Move right");
                }
                {
                    ImGui::TableNextColumn(); ImGui::Text("R");
                    ImGui::TableNextColumn(); ImGui::TextWrapped("Move upwards");
                }
                {
                    ImGui::TableNextColumn(); ImGui::Text("F");
                    ImGui::TableNextColumn(); ImGui::TextWrapped("Move downwards");
                }
                {
                    ImGui::TableNextColumn(); ImGui::Text("1");
                    ImGui::TableNextColumn(); ImGui::TextWrapped("Select block 1 in inventory");
                }
                {
                    ImGui::TableNextColumn(); ImGui::Text("2");
                    ImGui::TableNextColumn(); ImGui::TextWrapped("Select block 2 in inventory");
                }
                {
                    ImGui::TableNextColumn(); ImGui::Text("3");
                    ImGui::TableNextColumn(); ImGui::TextWrapped("Select block 3 in inventory");
                }
                {
                    ImGui::TableNextColumn(); ImGui::Text("4");
                    ImGui::TableNextColumn(); ImGui::TextWrapped("Select block 4 in inventory");
                }
                {
                    ImGui::TableNextColumn(); ImGui::Text("Left click");
                    ImGui::TableNextColumn(); ImGui::TextWrapped("Remove block on selected surface");
                }
                {
                    ImGui::TableNextColumn(); ImGui::Text("Right click");
                    ImGui::TableNextColumn(); ImGui::TextWrapped("Place equipped block on selected surface");
                }
                {
                    ImGui::TableNextColumn(); ImGui::Text("Esc");
                    ImGui::TableNextColumn(); ImGui::TextWrapped("Toggle menu (menu freezes viewport layer inputs)");
                }
                {
                    ImGui::TableNextColumn(); ImGui::Text("F3");
                    ImGui::TableNextColumn(); ImGui::TextWrapped("Toggle statistics overlay");
                }

                ImGui::EndTable();
            }
        }

        ImGui::EndTabItem();
    }
}
void Gui::displayMenuInfo()
{
    if(ImGui::BeginTabItem("Info"))
    {
        ImGui::SeparatorText("Info");

        ImGui::TextWrapped("Jamblox is a simple scaled down version of Minecraft");

        ImGui::SeparatorText("Credits");
        ImGui::Text("Organisation: MOV-sw");
        ImGui::Text("Owner: Maxim Varea");
        ImGui::Text("Designer: Maxim Varea");
        ImGui::Text("Developer: Maxim Varea");
        ImGui::Text("Tester: Maxim Varea");
        ImGui::Text("Documenter: Maxim Varea");

        ImGui::SeparatorText("Licence");

        ImGui::Text
("\n\
Apache Jamblox \n\
Copyright 2021 The Apache Software Foundation. \n\
\n\
This product includes software developed at \n\
The Apache Software Foundation (http://www.apache.org/). \n\
\n");

        ImGui::EndTabItem();
    }
}
void Gui::displayStatistics()
{
    const ImGuiViewport* viewport = ImGui::GetMainViewport();
    ImGui::SetNextWindowPos(ImVec2(0, 0), 0);
    ImGui::SetNextWindowSize(ImVec2(viewport->WorkSize.x, viewport->WorkSize.y), 0);

    ImGui::Begin
        ( "Statistics"
        , nullptr
        , ImGuiWindowFlags_NoCollapse
        | ImGuiWindowFlags_NoResize
        | ImGuiWindowFlags_NoBackground
        | ImGuiWindowFlags_NoBringToFrontOnFocus
        | ImGuiWindowFlags_NoFocusOnAppearing
        | ImGuiWindowFlags_NoNavFocus
        | ImGuiWindowFlags_NoMouseInputs
        | ImGuiWindowFlags_NoTitleBar
    );

    const float indent = 16.0f;

    {
        ImGui::Text("Application:");
        ImGui::Indent(indent);

        const ImGuiIO& io = ImGui::GetIO();
        ImGui::Text("Elapsed time: %.5fs", io.DeltaTime);
        ImGui::Text("Frequency: %.2fHz", 1.0f / io.DeltaTime);
        ImGui::Text("Framerate: %.2ffps", io.Framerate);

        ImGui::Unindent(indent);
    }
    {
        ImGui::Text("World:");
        ImGui::Indent(indent);

        {
            ImGui::Text("Chunks:");
            ImGui::Indent(indent);

            ImGui::Text("Render distance: %i", settings.renderDistance.retrieve());
            ImGui::Text("Loaded chunk count: %zu", statistics.chunkLocs.size());

            ImGui::Unindent(indent);
        }
        {
            const Lighting& lighting = statistics.lighting;
            ImGui::Text("Lighting:");
            ImGui::Indent(indent);

            {
                const Lighting::Ambient& ambient = lighting.ambient;
                ImGui::Text("Ambient:");
                ImGui::Indent(indent);

                const cx::Vector4f& color = ambient.color;
                ImGui::Text("Color: (%.2f, %.2f, %.2f, %.2f)", color.r, color.g, color.b, color.a);

                ImGui::Unindent(indent);
            }
            {
                const Lighting::Diffuse& diffuse = lighting.diffuse;
                ImGui::Text("Diffuse:");
                ImGui::Indent(indent);

                const cx::Vector3f& direction = diffuse.direction;
                ImGui::Text("Direction: (%.2f, %.2f, %.2f)", direction.x, direction.y, direction.z);

                const cx::Vector4f& color = diffuse.color;
                ImGui::Text("Color: (%.2f, %.2f, %.2f, %.2f)", color.r, color.g, color.b, color.a);

                ImGui::Unindent(indent);
            }

            ImGui::Unindent(indent);
        }

        ImGui::Unindent(indent);
    }
    {
        const Player& player = statistics.player;
        ImGui::Text("Player:");
        ImGui::Indent(indent);

        {
            const cx::Position& position = player.position;
            ImGui::Text("Position: (%.2f, %.2f, %.2f)", position.x, position.y, position.z);
        }
        {
            ChunkLoc chunkLoc = Chunk::getContainingChunk(player.position);
            ImGui::Text("Chunk location: (%i, %i)", chunkLoc.x, chunkLoc.y);
        }
        {
            const cx::Rotation& rotation = player.rotation;
            ImGui::Text("Rotation: (%.2f, %.2f, %.2f)", rotation.yaw, rotation.pitch, rotation.roll);
        }
        {
            const cx::Axes& axes = player.axes();
            ImGui::Text("Axes:");
            ImGui::Indent(indent);

            const cx::Direction& forward = axes.forward;
            ImGui::Text("Forward vector: (%.2f, %.2f, %.2f)", forward.x, forward.y, forward.z);
            const cx::Direction& up = axes.up;
            ImGui::Text("Up vector: (%.2f, %.2f, %.2f)", up.x, up.y, up.z);
            const cx::Direction& right = axes.right;
            ImGui::Text("Right vector: (%.2f, %.2f, %.2f)", right.x, right.y, right.z);

            ImGui::Unindent(indent);
        }

        ImGui::Unindent(indent);
    }

    ImGui::End();
}
