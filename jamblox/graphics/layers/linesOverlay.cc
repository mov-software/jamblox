// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

#include "linesOverlay.hh"

#include <jamblox/config.h>

LinesOverlay::LinesOverlay(Renderer& renderer, cx::Vector4f color, cx::decimal lineThickness)
    : Layer(renderer)
    , samples(renderer.samples)
    , renderPass(renderer.renderPass)
    , color(color)
    , lineThickness(lineThickness)
{
    cx::Workload workload;

    workload.add(&LinesOverlay::createVertexShaderModule, this, WORKING_DIR "/shaders/lines/shader.vert.spv");
    workload.add(&LinesOverlay::createFragmentShaderModule, this, WORKING_DIR "/shaders/lines/shader.frag.spv");
    workload.add(&LinesOverlay::createDescriptorSets, this);
    workload.add(&LinesOverlay::createGraphicsPipeline, this);
    workload.add(&LinesOverlay::createUniformBuffers, this);
    workload.add(&LinesOverlay::createSecondaryCommandPool, this);
    workload.add(&LinesOverlay::allocateCommandBuffer, this);
}

void LinesOverlay::createVertexShaderModule(const std::string& filePath)
{
    vertexShaderModule.initialize
        ( std::make_shared<cx::rhi::vk::ShaderModule>
            ( device.retrieve()
            , cx::rhi::vk::readShaderSpv(filePath)
        )
    );
}
void LinesOverlay::createFragmentShaderModule(const std::string& filePath)
{
    fragmentShaderModule.initialize
        ( std::make_shared<cx::rhi::vk::ShaderModule>
            ( device.retrieve()
            , cx::rhi::vk::readShaderSpv(filePath)
        )
    );
}

void LinesOverlay::createDescriptorPool()
{
    descriptorPool.initialize
        ( std::make_shared<cx::rhi::vk::DescriptorPool>
            ( device.retrieve()
            , swapchain.retrieve()->imageCount()
            , std::vector<VkDescriptorPoolSize>
            {
                {
                    .type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                    .descriptorCount = static_cast<uint32_t>(swapchain.retrieve()->imageCount()),
                },
            }
        )
    );
}
void LinesOverlay::createDescriptorSetLayouts()
{
    const std::vector<VkDescriptorSetLayoutBinding> bindings
    {
        {
            .binding = 0,
            .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            .descriptorCount = 1,
            .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
            .pImmutableSamplers = nullptr,
        },
    };

    std::vector<std::shared_ptr<cx::rhi::vk::DescriptorSetLayout>> descriptorSetLayoutsLocal(swapchain.retrieve()->imageCount());

    std::generate
        ( descriptorSetLayoutsLocal.begin()
        , descriptorSetLayoutsLocal.end()
        , [this, bindings]() -> std::shared_ptr<cx::rhi::vk::DescriptorSetLayout>
        {
            return std::make_shared<cx::rhi::vk::DescriptorSetLayout>(device.retrieve(), bindings);
        }
    );

    descriptorSetLayouts.initialize(descriptorSetLayoutsLocal);
}
void LinesOverlay::allocateDescriptorSets()
{
    descriptorSets.initialize(descriptorPool.retrieve()->allocateSets(descriptorSetLayouts.retrieve()));
}
void LinesOverlay::createDescriptorSets()
{
    createDescriptorPool();
    createDescriptorSetLayouts();
    allocateDescriptorSets();
}
void LinesOverlay::createGraphicsPipeline()
{
    const std::vector<VkPushConstantRange> pushConstantRanges {};

    const std::vector<VkPipelineShaderStageCreateInfo> shaderStages
    {
        {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .stage = VK_SHADER_STAGE_VERTEX_BIT,
            .module = vertexShaderModule.retrieve()->handle,
            .pName = "main",
            .pSpecializationInfo = nullptr,
        },
        {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .stage = VK_SHADER_STAGE_FRAGMENT_BIT,
            .module = fragmentShaderModule.retrieve()->handle,
            .pName = "main",
            .pSpecializationInfo = nullptr,
        },
    };

    const VkVertexInputBindingDescription bindingDescription
    {
        .binding = 0,
        .stride = sizeof(Vertex),
        .inputRate = VK_VERTEX_INPUT_RATE_VERTEX,
    };
    const std::vector<VkVertexInputAttributeDescription> attributeDescriptions
    {
        {
            .location = 0,
            .binding = 0,
            .format = VK_FORMAT_R32G32_SFLOAT,
            .offset = 0,
        },
    };
    const VkPipelineVertexInputStateCreateInfo vertexInputState
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .vertexBindingDescriptionCount = 1,
        .pVertexBindingDescriptions = &bindingDescription,
        .vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size()),
        .pVertexAttributeDescriptions = attributeDescriptions.data(),
    };
    const VkPipelineInputAssemblyStateCreateInfo inputAssemblyState
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .topology = VK_PRIMITIVE_TOPOLOGY_LINE_LIST,
        .primitiveRestartEnable = VK_FALSE,
    };

    VkExtent2D area;
    VkOffset2D offset;
    if(resolution.width > resolution.height)
    {
        area = resolution;
        offset =
        {
            .x = static_cast<int>(area.width - area.height) / 2,
            .y = 0,
        };
    }
    else
    {
        area =
        {
            .width = resolution.height,
            .height = resolution.width,
        };
        offset =
        {
            .x = 0,
            .y = static_cast<int>(area.width - area.height) / 2,
        };
    }

    const VkViewport viewport
    {
        .x = static_cast<float>(offset.x),
        .y = static_cast<float>(offset.y),
        .width = static_cast<float>(area.height),
        .height = static_cast<float>(area.height),
        .minDepth = 0.0f,
        .maxDepth = 1.0f,
    };
    const VkRect2D scissor
    {
        .offset = { 0, 0 },
        .extent = resolution,
    };
    const VkPipelineViewportStateCreateInfo viewportState
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .viewportCount = 1,
        .pViewports = &viewport,
        .scissorCount = 1,
        .pScissors = &scissor,
    };
    const VkPipelineRasterizationStateCreateInfo rasterizationState
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .depthClampEnable = VK_FALSE,
        .rasterizerDiscardEnable = VK_FALSE,
        .polygonMode = VK_POLYGON_MODE_FILL,
        .cullMode = VK_CULL_MODE_NONE,
        .frontFace = VK_FRONT_FACE_CLOCKWISE,
        .depthBiasEnable = VK_FALSE,
        .depthBiasConstantFactor = 0.0f,
        .depthBiasClamp = 0.0f,
        .depthBiasSlopeFactor = 0.0f,
        .lineWidth = static_cast<float>(lineThickness),
    };
    const VkPipelineMultisampleStateCreateInfo multisampleState
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .rasterizationSamples = samples,
        .sampleShadingEnable = VK_FALSE,
        .minSampleShading = 1.0f,
        .pSampleMask = nullptr,
        .alphaToCoverageEnable = VK_FALSE,
        .alphaToOneEnable = VK_FALSE,
    };
    const VkPipelineDepthStencilStateCreateInfo depthStencilState
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .depthTestEnable = VK_TRUE,
        .depthWriteEnable = VK_TRUE,
        .depthCompareOp = VK_COMPARE_OP_LESS,
        .depthBoundsTestEnable = VK_FALSE,
        .stencilTestEnable = VK_FALSE,
        .front = {},
        .back = {},
        .minDepthBounds = 0.0f,
        .maxDepthBounds = 0.0f,
    };
    const VkPipelineColorBlendAttachmentState colorAttachment
    {
        .blendEnable = VK_TRUE,
        .srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA,
        .dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,
        .colorBlendOp = VK_BLEND_OP_ADD,
        .srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE,
        .dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO,
        .alphaBlendOp = VK_BLEND_OP_ADD,
        .colorWriteMask
            = VK_COLOR_COMPONENT_R_BIT
            | VK_COLOR_COMPONENT_G_BIT
            | VK_COLOR_COMPONENT_B_BIT
            | VK_COLOR_COMPONENT_A_BIT,
    };
    const VkPipelineColorBlendStateCreateInfo colorBlendState
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .logicOpEnable = VK_FALSE,
        .logicOp = VK_LOGIC_OP_COPY,
        .attachmentCount = 1,
        .pAttachments = &colorAttachment,
        .blendConstants =
        {
            0.0f,
            0.0f,
            0.0f,
            0.0f,
        },
    };

    graphicsPipeline.initialize
        ( std::make_shared<cx::rhi::vk::GraphicsPipeline>
            ( device.retrieve()
            , renderPass.retrieve()
            , 0
            , descriptorSetLayouts.retrieve()
            , pushConstantRanges
            , shaderStages
            , &vertexInputState
            , &inputAssemblyState
            , nullptr
            , &viewportState
            , &rasterizationState
            , &multisampleState
            , &depthStencilState
            , &colorBlendState
            , nullptr
        )
    );
}

void LinesOverlay::createUniformBuffers()
{
    std::vector<std::shared_ptr<cx::rhi::vk::UniformBuffer>> uniformBuffersLocal(swapchain.retrieve()->imageCount());

    cx::TgrInitializeLock uniformBufferRegionManagerLock(uniformBufferRegionManager);
    std::unique_ptr<cx::rhi::vk::UniformBufferRegionManager>& uniformBufferRegionManager
    = this->uniformBufferRegionManager.retrieve
        ( uniformBufferRegionManagerLock
        , std::make_unique<cx::rhi::vk::UniformBufferRegionManager>(device.retrieve()->physical)
    );

    uniformBufferRegionManager->add<cx::Vector4f>(0);

    for(unsigned int i = 0u; i < swapchain.retrieve()->imageCount(); ++i)
    {
        uniformBuffersLocal[i] = std::make_shared<cx::rhi::vk::UniformBuffer>
            ( device.retrieve()
            , std::vector<uint32_t>
            {
                graphicsQueue.retrieve()->family.index,
            }
            , uniformBufferRegionManager->size()
        );
    }

    uniformBuffers.initialize(uniformBuffersLocal);
}
void LinesOverlay::createSecondaryCommandPool()
{
    secondaryCommandPool.initialize
        ( std::make_shared<cx::rhi::vk::CommandPool>
            ( device.retrieve()
            , graphicsQueue.retrieve()->family.index
            , false
        )
    );
}
void LinesOverlay::allocateCommandBuffer()
{
    cmd.initialize(secondaryCommandPool.retrieve()->allocateBuffer(VK_COMMAND_BUFFER_LEVEL_SECONDARY));
}

void LinesOverlay::addLines(const std::vector<Vertex>& lines)
{
    vertexes.insert(vertexes.end(), lines.cbegin(), lines.cend());
}
void LinesOverlay::loadVertexes(cx::Ref<cx::rhi::vk::SubmissionQueue> submissionQueue)
{
    vertexBuffer.initialize
        ( std::make_unique<cx::rhi::vk::VertexBuffer>
            ( device.retrieve()
            , std::vector<uint32_t>
            {
                graphicsQueue.retrieve()->family.index,
                transferQueue.retrieve()->family.index,
            }
            , submissionQueue
            , vertexes.size() * sizeof(Vertex)
            , vertexes.data()
        )
    );

    vertexCount = vertexes.size();
    vertexes.clear();
}

void LinesOverlay::loadDescriptorSets()
{
    for(unsigned int frame = 0u; frame < swapchain.retrieve()->imageCount(); ++frame)
    {
        cx::rhi::vk::UniformBuffer& uniformBuffer = *uniformBuffers.retrieve()[frame];
        const cx::rhi::vk::DescriptorSet descriptorSet = descriptorSets.retrieve()[frame];

        cx::TgrWriteLock uniformBufferRegionManagerLock(uniformBufferRegionManager);
        cx::rhi::vk::UniformBufferRegionManager& uniformBufferRegionManager = *this->uniformBufferRegionManager.retrieve(uniformBufferRegionManagerLock);
        uniformBufferRegionManager.load(uniformBuffer, descriptorSet);
    }
}

void LinesOverlay::recreateSwapchainResources()
{
    graphicsPipeline.reset();

    createGraphicsPipeline();
    recordDraw();
}
void LinesOverlay::recordDraw()
{
    cx::TgrWriteLock cmdLock(cmd);
    cx::rhi::vk::CommandBuffer& cmd = this->cmd.retrieve(cmdLock);

    cmd.begin(VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT);

    cmd.bindPipeline(*graphicsPipeline.retrieve());
    cmd.bindDescriptorSets(*graphicsPipeline.retrieve(), 0, descriptorSets.retrieve());

    cmd.bindVertexBuffer(0, *vertexBuffer.retrieve());
    cmd.draw(vertexCount, 1, 0, 0);

    cmd.end();
}
void LinesOverlay::submitCommands(cx::rhi::vk::CommandBuffer primary)
{
    primary.submitSecondary(cmd.retrieve());
}
void LinesOverlay::update(unsigned int i)
{
    cx::TgrReadLock uniformBufferRegionManagerLock(uniformBufferRegionManager);
    const cx::rhi::vk::UniformBufferRegionManager& uniformBufferRegionManager = *this->uniformBufferRegionManager.retrieve(uniformBufferRegionManagerLock);

    const cx::rhi::vk::BufferRegion& colorRegion = uniformBufferRegionManager[0];

    cx::TgrWriteLock uniformBuffersLock(uniformBuffers);
    std::shared_ptr<cx::rhi::vk::UniformBuffer>& uniformBuffer = uniformBuffers.retrieve(uniformBuffersLock)[i];

    uniformBuffer->load(&color, colorRegion.size, colorRegion.offset);
}

std::vector<LinesOverlay::Vertex> LinesOverlay::produceCrossHair(float size)
{
    return
        { { -size,  0.0f }
        , { +size,  0.0f }
        , {  0.0f, -size }
        , {  0.0f, +size },
    };
}

std::vector<LinesOverlay::Vertex> LinesOverlay::produceSquare(const cx::Vector2f& centre, float radius)
{
    return
        { { centre.x - radius, centre.y - radius }
        , { centre.x - radius, centre.y + radius }
        , { centre.x - radius, centre.y + radius }
        , { centre.x + radius, centre.y + radius }
        , { centre.x + radius, centre.y + radius }
        , { centre.x + radius, centre.y - radius }
        , { centre.x + radius, centre.y - radius }
        , { centre.x - radius, centre.y - radius }
    };
}
