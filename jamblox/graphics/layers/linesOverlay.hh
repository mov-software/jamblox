// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

#ifndef JAMBLOX_GRAPHICS_LAYERS_LINES_OVERLAY_HH
#define JAMBLOX_GRAPHICS_LAYERS_LINES_OVERLAY_HH

#include <jamblox/graphics/renderer.hh>

class LinesOverlay : public cx::Layer
{
public:
    typedef cx::Vector2f Vertex;
private:
    const VkSampleCountFlagBits& samples;

    cx::Tgr<std::shared_ptr<cx::rhi::vk::RenderPass>>& renderPass;

    cx::Tgr<std::shared_ptr<cx::rhi::vk::ShaderModule>>
        vertexShaderModule,
        fragmentShaderModule;

    cx::Tgr<std::shared_ptr<cx::rhi::vk::DescriptorPool>> descriptorPool;
    cx::Tgr<std::vector<std::shared_ptr<cx::rhi::vk::DescriptorSetLayout>>> descriptorSetLayouts;
    cx::Tgr<std::vector<cx::rhi::vk::DescriptorSet>> descriptorSets;

    cx::Tgr<std::shared_ptr<cx::rhi::vk::GraphicsPipeline>> graphicsPipeline;

    cx::Tgr<std::unique_ptr<cx::rhi::vk::UniformBufferRegionManager>> uniformBufferRegionManager;
    cx::Tgr<std::vector<std::shared_ptr<cx::rhi::vk::UniformBuffer>>> uniformBuffers;

    cx::Tgr<std::shared_ptr<cx::rhi::vk::CommandPool>> secondaryCommandPool;
    cx::Tgr<cx::rhi::vk::CommandBuffer> cmd;

    const cx::Vector4f color;
    const cx::decimal lineThickness;

    unsigned int vertexCount = 0u;
    std::vector<Vertex> vertexes;
    cx::Tgr<std::unique_ptr<cx::rhi::vk::VertexBuffer>> vertexBuffer;
public:
    LinesOverlay(Renderer& renderer, cx::Vector4f color, cx::decimal lineThickness);
private:
    void createVertexShaderModule(const std::string& filePath);
    void createFragmentShaderModule(const std::string& filePath);
    void createDescriptorPool();
    void createDescriptorSetLayouts();
    void allocateDescriptorSets();
    void createDescriptorSets();
    void createGraphicsPipeline();
    void createUniformBuffers();
    void createSecondaryCommandPool();
    void allocateCommandBuffer();
public:
    void addLines(const std::vector<Vertex>& lines);
    void loadVertexes(cx::Ref<cx::rhi::vk::SubmissionQueue> submissionQueue);
    void loadDescriptorSets();

    void recreateSwapchainResources() override;
    void recordDraw();
    void submitCommands(cx::rhi::vk::CommandBuffer primary) override;
    void update(unsigned int i) override;

    static std::vector<Vertex> produceCrossHair(float size);
    static std::vector<Vertex> produceSquare(const cx::Vector2f& centre, float radius);
};

#endif // JAMBLOX_GRAPHICS_LAYERS_LINES_OVERLAY_HH
