// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

#ifndef JAMBLOX_GRAPHICS_LAYERS_GUI_HH
#define JAMBLOX_GRAPHICS_LAYERS_GUI_HH

#include <jamblox/graphics/renderer.hh>

#include <jamblox/core/settings.hh>
#include <jamblox/core/statistics.hh>

#include <imgui/imgui.h>

class Gui : public cx::Layer
{
private:
    Settings& settings;
    Statistics& statistics;

    cx::Tgr<std::shared_ptr<cx::rhi::vk::DescriptorPool>> descriptorPool;

    ImDrawData* drawData = nullptr;

    bool
        menuOpen = false,
        statisticsOpen = false;
public:
    Gui
        ( Renderer& renderer
        , cx::Ref<cx::io::glfw::Window> window
        , cx::Ref<cx::rhi::vk::Instance> instance
        , Settings& settings
        , Statistics& statistics
    );

    ~Gui();

    void showMenu();
    void hideMenu();
    void toggleStats();
private:
    void createDescriptorPool();
public:
    void recreateSwapchainResources() override;
    void submitCommands(cx::rhi::vk::CommandBuffer primary) override;
    void update(unsigned int) override;
private:
    void displayMenu();
    void displayMenuSettings();
    void displayMenuHelp();
    void displayMenuInfo();
    void displayStatistics();
};

#endif // JAMBLOX_GRAPHICS_LAYERS_GUI_HH
