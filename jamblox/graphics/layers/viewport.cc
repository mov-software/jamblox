// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

#include "viewport.hh"

#include <jamblox/world/chunk.hh>

Viewport::Viewport(Renderer& renderer, Camera& camera, Lighting& lighting)
    : Layer(renderer)
    , camera(camera)
    , samples(renderer.samples)
    , renderPass(renderer.renderPass)
    , lighting(lighting)
{
    cx::Workload workload;

    workload.add(&Viewport::createVertexShaderModule, this, WORKING_DIR "/shaders/cubes/shader.vert.spv");
    workload.add(&Viewport::createFragmentShaderModule, this, WORKING_DIR "/shaders/cubes/shader.frag.spv");
    workload.add(&Viewport::createGeometryShaderModule, this, WORKING_DIR "/shaders/cubes/shader.geom.spv");
    workload.add(&Viewport::createDescriptorSets, this);
    workload.add(&Viewport::createGraphicsPipeline, this);
    workload.add(&Viewport::createUniformBuffers, this);
    workload.add(&Viewport::createSecondaryCommandPool, this);
    workload.add(&Viewport::allocateCommandBuffer, this);
}

void Viewport::createVertexShaderModule(const std::string& filePath)
{
    vertexShaderModule.initialize
        ( std::make_shared<cx::rhi::vk::ShaderModule>
            ( device.retrieve()
            , cx::rhi::vk::readShaderSpv(filePath)
        )
    );
}
void Viewport::createFragmentShaderModule(const std::string& filePath)
{
    fragmentShaderModule.initialize
        ( std::make_shared<cx::rhi::vk::ShaderModule>
            ( device.retrieve()
            , cx::rhi::vk::readShaderSpv(filePath)
        )
    );
}
void Viewport::createGeometryShaderModule(const std::string& filePath)
{
    geometryShaderModule.initialize
        ( std::make_shared<cx::rhi::vk::ShaderModule>
            ( device.retrieve()
            , cx::rhi::vk::readShaderSpv(filePath)
        )
    );
}
void Viewport::createDescriptorPool()
{
    descriptorPool.initialize
        ( std::make_shared<cx::rhi::vk::DescriptorPool>
            ( device.retrieve()
            , swapchain.retrieve()->imageCount()
            , std::vector<VkDescriptorPoolSize>
            {
                {
                    .type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                    .descriptorCount = static_cast<uint32_t>(swapchain.retrieve()->imageCount()),
                },
                {
                    .type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                    .descriptorCount = static_cast<uint32_t>(swapchain.retrieve()->imageCount()),
                },
                {
                    .type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                    .descriptorCount = static_cast<uint32_t>(swapchain.retrieve()->imageCount()),
                },
                {
                    .type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                    .descriptorCount = static_cast<uint32_t>(swapchain.retrieve()->imageCount()),
                },
            }
        )
    );
}
void Viewport::createDescriptorSetLayouts()
{
    const std::vector<VkDescriptorSetLayoutBinding> bindings
    {
        {
            .binding = 0,
            .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            .descriptorCount = 1,
            .stageFlags = VK_SHADER_STAGE_GEOMETRY_BIT,
            .pImmutableSamplers = nullptr,
        },
        {
            .binding = 1,
            .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
            .descriptorCount = 1,
            .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
            .pImmutableSamplers = nullptr,
        },
        {
            .binding = 2,
            .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            .descriptorCount = 1,
            .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
            .pImmutableSamplers = nullptr,
        },
        {
            .binding = 3,
            .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            .descriptorCount = 1,
            .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
            .pImmutableSamplers = nullptr,
        },
    };

    std::vector<std::shared_ptr<cx::rhi::vk::DescriptorSetLayout>> descriptorSetLayoutsLocal(swapchain.retrieve()->imageCount());

    std::generate
        ( descriptorSetLayoutsLocal.begin()
        , descriptorSetLayoutsLocal.end()
        , [this, bindings]() -> std::shared_ptr<cx::rhi::vk::DescriptorSetLayout>
        {
            return std::make_shared<cx::rhi::vk::DescriptorSetLayout>(device.retrieve(), bindings);
        }
    );

    descriptorSetLayouts.initialize(descriptorSetLayoutsLocal);
}
void Viewport::allocateDescriptorSets()
{
    descriptorSets.initialize(descriptorPool.retrieve()->allocateSets(descriptorSetLayouts.retrieve()));
}
void Viewport::createDescriptorSets()
{
    createDescriptorPool();
    createDescriptorSetLayouts();
    allocateDescriptorSets();
}
void Viewport::createGraphicsPipeline()
{
    const std::vector<VkPushConstantRange> pushConstantRanges {};

    const std::vector<VkPipelineShaderStageCreateInfo> shaderStages
    {
        {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .stage = VK_SHADER_STAGE_VERTEX_BIT,
            .module = vertexShaderModule.retrieve()->handle,
            .pName = "main",
            .pSpecializationInfo = nullptr,
        },
        {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .stage = VK_SHADER_STAGE_GEOMETRY_BIT,
            .module = geometryShaderModule.retrieve()->handle,
            .pName = "main",
            .pSpecializationInfo = nullptr,
        },
        {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .stage = VK_SHADER_STAGE_FRAGMENT_BIT,
            .module = fragmentShaderModule.retrieve()->handle,
            .pName = "main",
            .pSpecializationInfo = nullptr,
        },
    };

    const VkVertexInputBindingDescription bindingDescription
    {
        .binding = 0,
        .stride = sizeof(Vertex),
        .inputRate = VK_VERTEX_INPUT_RATE_VERTEX,
    };
    const std::vector<VkVertexInputAttributeDescription> attributeDescriptions
    {
        {
            .location = 0,
            .binding = 0,
            .format = VK_FORMAT_R32G32B32_SINT,
            .offset = offsetof(Vertex, position),
        },
        {
            .location = 1,
            .binding = 0,
            .format = VK_FORMAT_R32_UINT,
            .offset = offsetof(Vertex, faces),
        },
        {
            .location = 2,
            .binding = 0,
            .format = VK_FORMAT_R32_UINT,
            .offset = offsetof(Vertex, block),
        },
    };
    const VkPipelineVertexInputStateCreateInfo vertexInputState
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .vertexBindingDescriptionCount = 1,
        .pVertexBindingDescriptions = &bindingDescription,
        .vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size()),
        .pVertexAttributeDescriptions = attributeDescriptions.data(),
    };
    const VkPipelineInputAssemblyStateCreateInfo inputAssemblyState
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .topology = VK_PRIMITIVE_TOPOLOGY_POINT_LIST,
        .primitiveRestartEnable = VK_FALSE,
    };
    const VkViewport viewport
    {
        .x = 0.0f,
        .y = 0.0f,
        .width = static_cast<float>(resolution.width),
        .height = static_cast<float>(resolution.height),
        .minDepth = 0.0f,
        .maxDepth = 1.0f,
    };
    const VkRect2D scissor
    {
        .offset = { 0, 0 },
        .extent = resolution,
    };
    const VkPipelineViewportStateCreateInfo viewportState
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .viewportCount = 1,
        .pViewports = &viewport,
        .scissorCount = 1,
        .pScissors = &scissor,
    };
    const VkPipelineRasterizationStateCreateInfo rasterizationState
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .depthClampEnable = VK_FALSE,
        .rasterizerDiscardEnable = VK_FALSE,
        .polygonMode = VK_POLYGON_MODE_FILL,
        .cullMode = VK_CULL_MODE_BACK_BIT,
        .frontFace = VK_FRONT_FACE_CLOCKWISE,
        .depthBiasEnable = VK_FALSE,
        .depthBiasConstantFactor = 0.0f,
        .depthBiasClamp = 0.0f,
        .depthBiasSlopeFactor = 0.0f,
        .lineWidth = 1.0f,
    };
    const VkPipelineMultisampleStateCreateInfo multisampleState
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .rasterizationSamples = samples,
        .sampleShadingEnable = VK_FALSE,
        .minSampleShading = 1.0f,
        .pSampleMask = nullptr,
        .alphaToCoverageEnable = VK_FALSE,
        .alphaToOneEnable = VK_FALSE,
    };
    const VkPipelineDepthStencilStateCreateInfo depthStencilState
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .depthTestEnable = VK_TRUE,
        .depthWriteEnable = VK_TRUE,
        .depthCompareOp = VK_COMPARE_OP_LESS,
        .depthBoundsTestEnable = VK_FALSE,
        .stencilTestEnable = VK_FALSE,
        .front = {},
        .back = {},
        .minDepthBounds = 0.0f,
        .maxDepthBounds = 0.0f,
    };
    const VkPipelineColorBlendAttachmentState colorAttachment
    {
        .blendEnable = VK_TRUE,
        .srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA,
        .dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,
        .colorBlendOp = VK_BLEND_OP_ADD,
        .srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE,
        .dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO,
        .alphaBlendOp = VK_BLEND_OP_ADD,
        .colorWriteMask
            = VK_COLOR_COMPONENT_R_BIT
            | VK_COLOR_COMPONENT_G_BIT
            | VK_COLOR_COMPONENT_B_BIT
            | VK_COLOR_COMPONENT_A_BIT,
    };
    const VkPipelineColorBlendStateCreateInfo colorBlendState
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .logicOpEnable = VK_FALSE,
        .logicOp = VK_LOGIC_OP_COPY,
        .attachmentCount = 1,
        .pAttachments = &colorAttachment,
        .blendConstants =
        {
            0.0f,
            0.0f,
            0.0f,
            0.0f,
        },
    };

    graphicsPipeline.initialize
        ( std::make_shared<cx::rhi::vk::GraphicsPipeline>
            ( device.retrieve()
            , renderPass.retrieve()
            , 0
            , descriptorSetLayouts.retrieve()
            , pushConstantRanges
            , shaderStages
            , &vertexInputState
            , &inputAssemblyState
            , nullptr
            , &viewportState
            , &rasterizationState
            , &multisampleState
            , &depthStencilState
            , &colorBlendState
            , nullptr
        )
    );
}

void Viewport::createUniformBuffers()
{
    std::vector<std::shared_ptr<cx::rhi::vk::UniformBuffer>> uniformBuffersLocal(swapchain.retrieve()->imageCount());

    cx::TgrInitializeLock uniformBufferRegionManagerLock(uniformBufferRegionManager);
    std::unique_ptr<cx::rhi::vk::UniformBufferRegionManager>& uniformBufferRegionManager
    = this->uniformBufferRegionManager.retrieve
        ( uniformBufferRegionManagerLock
        , std::make_unique<cx::rhi::vk::UniformBufferRegionManager>(device.retrieve()->physical)
    );

    uniformBufferRegionManager->add<cx::Matrix4x4f>(0);
    uniformBufferRegionManager->add<Lighting::Ambient>(2);
    uniformBufferRegionManager->add<Lighting::Diffuse>(3);

    for(unsigned int i = 0u; i < swapchain.retrieve()->imageCount(); ++i)
    {
        uniformBuffersLocal[i] = std::make_shared<cx::rhi::vk::UniformBuffer>
            ( device.retrieve()
            , std::vector<uint32_t>
            {
                graphicsQueue.retrieve()->family.index,
            }
            , uniformBufferRegionManager->size()
        );
    }

    uniformBuffers.initialize(uniformBuffersLocal);
}
void Viewport::createSecondaryCommandPool()
{
    secondaryCommandPool.initialize
        ( std::make_shared<cx::rhi::vk::CommandPool>
            ( device.retrieve()
            , graphicsQueue.retrieve()->family.index
            , false
        )
    );
}
void Viewport::allocateCommandBuffer()
{
    cmd.initialize(secondaryCommandPool.retrieve()->allocateBuffer(VK_COMMAND_BUFFER_LEVEL_SECONDARY));
}

cx::rhi::vk::VertexBuffer* Viewport::createVertexBuffer(const std::vector<Vertex>& vertexes, cx::Ref<cx::rhi::vk::SubmissionQueue> submissionQueue)
{
    return new cx::rhi::vk::VertexBuffer
        ( device.retrieve()
        , std::vector<uint32_t>
        {
            graphicsQueue.retrieve()->family.index,
            transferQueue.retrieve()->family.index,
        }
        , submissionQueue
        , vertexes.size() * sizeof(Vertex)
        , vertexes.data()
    );
}
void Viewport::bindChunks(const std::unordered_map<ChunkLoc, std::shared_ptr<Chunk>>& chunks)
{
    this->chunks.clear();

    std::transform
        ( chunks.cbegin()
        , chunks.cend()
        , std::back_inserter(this->chunks)
        , [](const std::pair<ChunkLoc, std::shared_ptr<Chunk>>& pair)
        {
            return pair.second;
        }
    );
}

void Viewport::loadTextureAtlas(const cx::Bitmap& bitmap, cx::Ref<cx::rhi::vk::SubmissionQueue> submissionQueue)
{
    float mipLevels = std::floor(std::log2(std::max(bitmap.width, bitmap.height))) + 1.0f;

    textureAtlas.initialize
        ( std::make_shared<cx::rhi::vk::Texture>
            ( std::vector<uint32_t>
            {
                graphicsQueue.retrieve()->family.index,
                transferQueue.retrieve()->family.index,
            }
            , submissionQueue
            , bitmap
            , static_cast<uint32_t>(mipLevels)
        )
    );

    sampler.initialize
        ( std::make_shared<cx::rhi::vk::Sampler>
            ( device.retrieve()
            , VK_FILTER_NEAREST
            , VK_FILTER_NEAREST
            , VK_SAMPLER_MIPMAP_MODE_NEAREST
            , VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER
            , VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER
            , VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER
            , VK_BORDER_COLOR_INT_OPAQUE_BLACK
            , 0.0f
            , 0.0f
            , mipLevels
        )
    );
}
void Viewport::loadDescriptorSets()
{
    for(unsigned int frame = 0u; frame < swapchain.retrieve()->imageCount(); ++frame)
    {
        cx::rhi::vk::UniformBuffer& uniformBuffer = *uniformBuffers.retrieve()[frame];
        const cx::rhi::vk::DescriptorSet descriptorSet = descriptorSets.retrieve()[frame];

        cx::TgrWriteLock uniformBufferRegionManagerLock(uniformBufferRegionManager);
        cx::rhi::vk::UniformBufferRegionManager& uniformBufferRegionManager = *this->uniformBufferRegionManager.retrieve(uniformBufferRegionManagerLock);
        uniformBufferRegionManager.load(uniformBuffer, descriptorSet);

        const VkDescriptorImageInfo imageInfo
        {
            .sampler = sampler.retrieve()->handle,
            .imageView = textureAtlas.retrieve()->imageView->handle,
            .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
        };
        const VkWriteDescriptorSet imageWriteDescriptorSet
        {
            .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            .pNext = nullptr,
            .dstSet = descriptorSet,
            .dstBinding = 1,
            .dstArrayElement = 0,
            .descriptorCount = 1,
            .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
            .pImageInfo = &imageInfo,
            .pBufferInfo = nullptr,
            .pTexelBufferView = nullptr,
        };

        vkUpdateDescriptorSets(device.retrieve()->handle, 1, &imageWriteDescriptorSet, 0, nullptr);
    }
}

void Viewport::recreateSwapchainResources()
{
    graphicsPipeline.reset();

    createGraphicsPipeline();
    recordDraw();
}
void Viewport::recordDraw()
{
    cx::TgrWriteLock cmdLock(cmd, std::defer_lock);
    cx::TgrReadLock graphicsPipelineLock(graphicsPipeline, std::defer_lock);

    std::lock(cmdLock, graphicsPipelineLock);

    cx::rhi::vk::CommandBuffer& cmd = this->cmd.retrieve(cmdLock);
    cx::rhi::vk::GraphicsPipeline& graphicsPipeline = *this->graphicsPipeline.retrieve(graphicsPipelineLock);

    cmd.begin(VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT);

    cmd.bindPipeline(graphicsPipeline);
    cmd.bindDescriptorSets(graphicsPipeline, 0, descriptorSets.retrieve());

    for(const std::shared_ptr<Chunk>& chunk : chunks)
    {
        cmd.bindVertexBuffer(0, chunk->vertexBuffer());
        cmd.draw(chunk->vertexes().size(), 1, 0, 0);
    }

    cmd.end();
}
void Viewport::submitCommands(cx::rhi::vk::CommandBuffer primary)
{
    primary.submitSecondary(cmd.retrieve());
}
void Viewport::update(unsigned int i)
{
    const cx::Matrix4x4f
        model = cx::identity(),
        view = cx::lookAt
            ( camera.position
            , camera.axes()
        ),
        projection = cx::perspective
            ( cx::degToRad(camera.frustum.fov)
            , static_cast<cx::decimal>(resolution.width) / static_cast<cx::decimal>(resolution.height)
            , camera.frustum.nearZ
            , camera.frustum.farZ
        );

    const cx::Matrix4x4f mvp
    {
        projection * view * model
    };

    cx::TgrReadLock uniformBufferRegionManagerLock(uniformBufferRegionManager);
    const cx::rhi::vk::UniformBufferRegionManager& uniformBufferRegionManager = *this->uniformBufferRegionManager.retrieve(uniformBufferRegionManagerLock);

    const cx::rhi::vk::BufferRegion
        &mvpRegion = uniformBufferRegionManager[0],
        &ambientLightingRegion = uniformBufferRegionManager[1],
        &diffuseLightingRegion = uniformBufferRegionManager[2];

    cx::TgrWriteLock uniformBuffersLock(uniformBuffers);
    std::shared_ptr<cx::rhi::vk::UniformBuffer>& uniformBuffer = uniformBuffers.retrieve(uniformBuffersLock)[i];

    uniformBuffer->load(&mvp, mvpRegion.size, mvpRegion.offset);
    uniformBuffer->load(&lighting.ambient, ambientLightingRegion.size, ambientLightingRegion.offset);
    uniformBuffer->load(&lighting.diffuse, diffuseLightingRegion.size, diffuseLightingRegion.offset);
}
