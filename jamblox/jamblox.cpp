// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

#include "jamblox.hh"

#include <jamblox/world/superflat.hh>

Jamblox::Jamblox(const cx::Application::Mode mode)
    : cx::Application("Jamblox", mode)
    , cx::io::glfw::Application()
    , cx::rhi::vk::Application(this, VK_MAKE_VERSION(1, 0, 0))
    , player
    (
        { CHUNK_WIDTH/2, 60, -CHUNK_DEPTH/2 },
        { 0, 0, 0 },
        {
            .fov = 45.0_D,
            .nearZ = 0.1_D,
            .farZ = 1000.0_D
        }
    )
    , lighting
    {
        .ambient =
        {
            .color = { 1.0f, 1.0f, 1.0f, 0.05f },
        },
        .diffuse =
        {
            .direction = { 0.0f, -1.0f, 0.0f },
            .color = { 1.0f, 1.0f, 1.0f, 0.2f },
        },
    }
    , settings
    {
        .renderDistance = 4,
        .movementSpeed = 5.0,
        .rotationSpeed = { 1.0, 2.0 },
    }
    , statistics
    {
        .chunkLocs = currentChunkLocs,
        .lighting = lighting,
        .player = player,
    }
{
    createWindow();

    cx::Workload workload;

    workload.add(&Jamblox::loadWorld, this);
    workload.add(&Jamblox::loadTextureAtlas, this, WORKING_DIR "/res/texture_atlas.png");
    workload.add(&Jamblox::selectPhysicalDevice, this, instance->getPhysicalDevices());
    workload.add(&Jamblox::getQueueFamilies, this);
    workload.add(&Jamblox::getTransferQueueFamily, this);
    workload.add(&Jamblox::getGraphicsQueueFamily, this);
    workload.add(&Jamblox::getPresentQueueFamily, this);
    workload.add(&Jamblox::addWindowEventListeners, this);
    workload.add(&Jamblox::createSurface, this);
    workload.add(&Jamblox::getSurfaceFormat, this);
    workload.add(&Jamblox::getPresentMode, this);
    workload.add(&Jamblox::getResolution, this);
    workload.add(&Jamblox::getSamples, this);
    workload.add(&Jamblox::createRenderer, this);
    workload.add(&Jamblox::createViewport, this);
    workload.add(&Jamblox::createLinesOverlay, this);
    workload.add(&Jamblox::createImageTilesOverlay, this);
    workload.add(&Jamblox::createGui, this);
    workload.add(&Jamblox::addLayers, this);
    workload.add(&Jamblox::loadResources, this);

    workload.wait();
}

Jamblox::~Jamblox() = default;

void Jamblox::main()
{
    linesOverlay.retrieve()->recordDraw();
    imageTilesOverlay.retrieve()->recordDraw();

    while(window->isOpen())
    {
        update();
    }
}

void Jamblox::update()
{
    static std::chrono::time_point<std::chrono::steady_clock> prev = std::chrono::steady_clock::now();

    glfwPollEvents();

    std::chrono::time_point<std::chrono::steady_clock> now = std::chrono::steady_clock::now();
    std::chrono::nanoseconds interval = now - prev;
    decimal t = interval.count() / std::pow(10, 9);
    prev = now;

    updateMovement(t);

    cx::TgrReadLock rendererLock(renderer);
    renderer.retrieve(rendererLock)->draw
        ( [this]() -> VkExtent2D
        {
            return window->resolution();
        }
    );
}

void Jamblox::updateMovement(decimal t)
{
    cx::TgrWriteLock rendererLock(renderer);
    Renderer& renderer = *this->renderer.retrieve(rendererLock);

    const decimal displacement = settings.movementSpeed.retrieve() * t;

    if(buttons[cx::io::glfw::W])
    {
        player.position.x += displacement * cx::decimal_cast(sin(player.rotation.yaw));
        player.position.z += displacement * cx::decimal_cast(cos(player.rotation.yaw));
    }
    if(buttons[cx::io::glfw::S])
    {
        player.position.x -= displacement * cx::decimal_cast(sin(player.rotation.yaw));
        player.position.z -= displacement * cx::decimal_cast(cos(player.rotation.yaw));
    }
    if(buttons[cx::io::glfw::D])
    {
        player.position.x += displacement * cx::decimal_cast(cos(player.rotation.yaw));
        player.position.z -= displacement * cx::decimal_cast(sin(player.rotation.yaw));
    }
    if(buttons[cx::io::glfw::A])
    {
        player.position.x -= displacement * cx::decimal_cast(cos(player.rotation.yaw));
        player.position.z += displacement * cx::decimal_cast(sin(player.rotation.yaw));
    }
    if(buttons[cx::io::glfw::R])
    {
        player.position.y += displacement;
    }
    if(buttons[cx::io::glfw::F])
    {
        player.position.y -= displacement;
    }

    if(!menu)
    {
        const Vector2d cursor = window->cursor();
        cx::Vector2D rotationSpeed = settings.rotationSpeed.retrieve();
        cx::Vector2d centralisedCursor = cursor - window->size().cast<double>()/2.0;

        player.rotation.yaw = cx::decimal_cast(2.0 * M_PI * rotationSpeed.x * centralisedCursor.x / window->size().x);
        player.rotation.pitch = cx::decimal_cast(std::clamp(M_PI * rotationSpeed.y * centralisedCursor.y / window->size().y, -M_PI/2.0, +M_PI/2.0));
    }

    rendererLock.unlock();

    updateVisibleChunks();

    player.updateAxes();
}

void Jamblox::selectPhysicalDevice(const std::vector<std::shared_ptr<cx::rhi::vk::PhysicalDevice>>& physicalDevices)
{
    physicalDevice.initialize(physicalDevices.front());
}
void Jamblox::getQueueFamilies()
{
    queueFamilies.initialize(physicalDevice.retrieve()->getQueueFamilies());
}
void Jamblox::getTransferQueueFamily()
{
    for(const cx::rhi::vk::QueueFamily& queueFamily : queueFamilies.retrieve())
    {
        if(queueFamily.capabilities & VK_QUEUE_TRANSFER_BIT)
        {
            transferQueueFamily.initialize(queueFamily);
            return;
        }
    }

    throw cx::rhi::vk::Exception("There are no queue families supporting transfer operations");
}
void Jamblox::getGraphicsQueueFamily()
{
    for(const cx::rhi::vk::QueueFamily& queueFamily : queueFamilies.retrieve())
    {
        if(queueFamily.capabilities & VK_QUEUE_GRAPHICS_BIT)
        {
            graphicsQueueFamily.initialize(queueFamily);
            return;
        }
    }

    throw cx::rhi::vk::Exception("There are no queue families supporting graphics operations");
}
void Jamblox::getPresentQueueFamily()
{
    for(const cx::rhi::vk::QueueFamily& queueFamily : queueFamilies.retrieve())
    {
        VkBool32 supportsSurface;
        vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice.retrieve()->handle, queueFamily.index, surface.retrieve()->handle, &supportsSurface);

        if(supportsSurface)
        {
            presentQueueFamily.initialize(queueFamily);
            return;
        }
    }

    throw cx::rhi::vk::Exception("There are no queue families supporting the window surface");
}

void Jamblox::createWindow()
{
    window = std::make_shared<cx::io::glfw::Window>
        ( 720
        , 480
        , "Jamblox"
    );

    window->cursor(window->size().cast<double>()/2.0);
    window->disableCursor();

    if(glfwRawMouseMotionSupported())
    {
        window->enableRawMouseMotion();
    }
}
void Jamblox::createSurface()
{
    surface.initialize
        ( std::shared_ptr<cx::rhi::vk::Surface>
            ( window->createSurface
                ( instance
                , physicalDevice.retrieve()
            )
        )
    );
}
void Jamblox::getSurfaceFormat()
{
    const std::vector<VkSurfaceFormatKHR>& formats = surface.retrieve()->formats();
    bool found = false;

    for(std::vector<VkSurfaceFormatKHR>::const_iterator iterator = formats.cbegin(); !found && iterator < formats.cend(); ++iterator)
    {
        if(iterator->format == VK_FORMAT_B8G8R8A8_SRGB && iterator->colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
        {
            surfaceFormat.initialize(*iterator);
            found = true;
        }
    }

    if(!found)
    {
        throw cx::rhi::vk::Exception("There are no surface formats supporting 8-bit BGRA and a non-linear colorspace");
    }
}
void Jamblox::getPresentMode()
{
    const std::vector<VkPresentModeKHR>& presentModes = surface.retrieve()->presentModes();

    const std::vector<VkPresentModeKHR>::const_iterator
        begin = presentModes.cbegin(),
        end = presentModes.cend();

    if(std::find(begin, end, VK_PRESENT_MODE_MAILBOX_KHR) != end)
    {
        presentMode.initialize(VK_PRESENT_MODE_MAILBOX_KHR);
    }
    else
    {
        cx::rhi::vk::logger.warn("There are no surface formats supporting mailbox presentation");

        if(std::find(begin, end, VK_PRESENT_MODE_FIFO_KHR) != end)
        {
            presentMode.initialize(VK_PRESENT_MODE_FIFO_KHR);
        }
        else
        {
            throw cx::rhi::vk::Exception("There are no surface formats supporting FIFO presentation");
        }
    }
}
void Jamblox::getResolution()
{
    resolution.initialize(surface.retrieve()->clampExtent(window->resolution()));
}
void Jamblox::getSamples()
{
    samples.initialize
        ( physicalDevice.retrieve()->getMaxSupportedFramebufferSampleCounts
            ( cx::rhi::vk::FRAMEBUFFER_COLOR_ATTACHMENT_BIT
            | cx::rhi::vk::FRAMEBUFFER_DEPTH_ATTACHMENT_BIT
        )
    );
}
void Jamblox::createRenderer()
{
    renderer.initialize
        ( std::make_unique<Renderer>
            ( physicalDevice.retrieve()
            , queueFamilies.retrieve()
            , transferQueueFamily.retrieve()
            , graphicsQueueFamily.retrieve()
            , presentQueueFamily.retrieve()
            , surface.retrieve()
            , surfaceFormat.retrieve()
            , presentMode.retrieve()
            , resolution.retrieve()
            , samples.retrieve()
        )
    );
}
void Jamblox::createViewport()
{
    cx::TgrWriteLock rendererLock(renderer, std::defer_lock);
    cx::TgrInitializeLock viewportLock(viewport, std::defer_lock);

    std::lock(rendererLock, viewportLock);

    Renderer& renderer = *this->renderer.retrieve(rendererLock);
    std::shared_ptr<Viewport>& viewport = this->viewport.retrieve(viewportLock);

    viewport = std::make_shared<Viewport>(renderer, player, lighting);
}
void Jamblox::createLinesOverlay()
{
    cx::TgrWriteLock rendererLock(renderer, std::defer_lock);
    cx::TgrInitializeLock linesOverlayLock(linesOverlay, std::defer_lock);

    std::lock(rendererLock, linesOverlayLock);

    Renderer& renderer = *this->renderer.retrieve(rendererLock);
    std::shared_ptr<LinesOverlay>& linesOverlay = this->linesOverlay.retrieve(linesOverlayLock);

    linesOverlay = std::make_shared<LinesOverlay>(renderer, cx::Vector4f{ 0.75f, 0.75f, 0.75f, 1.0f }, 4.0_D);
}
void Jamblox::createImageTilesOverlay()
{
    cx::TgrWriteLock rendererLock(renderer, std::defer_lock);
    cx::TgrInitializeLock imageTilesOverlayLock(imageTilesOverlay, std::defer_lock);

    std::lock(rendererLock, imageTilesOverlayLock);

    Renderer& renderer = *this->renderer.retrieve(rendererLock);
    std::shared_ptr<ImageTilesOverlay>& imageTilesOverlay = this->imageTilesOverlay.retrieve(imageTilesOverlayLock);

    imageTilesOverlay = std::make_shared<ImageTilesOverlay>(renderer);
}
void Jamblox::createGui()
{
    cx::TgrWriteLock rendererLock(renderer, std::defer_lock);
    cx::TgrInitializeLock guiLock(gui, std::defer_lock);

    std::lock(rendererLock, guiLock);

    Renderer& renderer = *this->renderer.retrieve(rendererLock);
    std::shared_ptr<Gui>& gui = this->gui.retrieve(guiLock);

    gui = std::make_shared<Gui>(renderer, window, instance, settings, statistics);
}
void Jamblox::addLayers()
{
    cx::TgrWriteLock rendererLock(renderer, std::defer_lock);
    cx::TgrReadLock
        viewportLock(viewport, std::defer_lock),
        linesOverlayLock(linesOverlay, std::defer_lock),
        imageTilesOverlayLock(imageTilesOverlay, std::defer_lock),
        guiLock(gui, std::defer_lock);

    std::lock
        ( rendererLock
        , viewportLock
        , linesOverlayLock
        , imageTilesOverlayLock
        , guiLock
    );

    Renderer& renderer = *this->renderer.retrieve(rendererLock);

    renderer.addLayer(viewport.retrieve(viewportLock));
    renderer.addLayer(linesOverlay.retrieve(linesOverlayLock));
    renderer.addLayer(imageTilesOverlay.retrieve(imageTilesOverlayLock));
    renderer.addLayer(gui.retrieve(guiLock));
}
void Jamblox::loadInventoryOverlayData(cx::Ref<cx::rhi::vk::SubmissionQueue> submissionQueue)
{
    cx::TgrWriteLock linesOverlayLock(linesOverlay, std::defer_lock);
    cx::TgrWriteLock imageTilesOverlayLock(imageTilesOverlay, std::defer_lock);

    std::lock(linesOverlayLock, imageTilesOverlayLock);

    LinesOverlay& linesOverlay = *this->linesOverlay.retrieve(linesOverlayLock);
    ImageTilesOverlay& imageTilesOverlay = *this->imageTilesOverlay.retrieve(imageTilesOverlayLock);

    linesOverlay.addLines(LinesOverlay::produceCrossHair(0.05f));

    float
        x = inventoryTileSize * static_cast<float>(1 - static_cast<int>(Inventory::size)) / 2,
        outerRadius = inventoryTileSize / 2,
        innerRadius = (inventoryTileSize - inventoryTilePadding) / 2;

    std::array<cx::Vector2f, Inventory::size> centres;
    for(unsigned int i = 0u; i < Inventory::size; ++i)
    {
        cx::Vector2f centre = { x, inventoryVerticalPosition };
        linesOverlay.addLines(LinesOverlay::produceSquare(centre, outerRadius));
        imageTilesOverlay.addTile({ centre, innerRadius, player.inventory[i] });
        centres[i] = centre;
        x += inventoryTileSize;
    }

    outerRadius += 0.01f;
    switch(player.inventory.index)
    {
        case 0u:
            linesOverlay.addLines(LinesOverlay::produceSquare(centres[0], outerRadius));
            break;
        case 1u:
            linesOverlay.addLines(LinesOverlay::produceSquare(centres[1], outerRadius));
            break;
        case 2u:
            linesOverlay.addLines(LinesOverlay::produceSquare(centres[2], outerRadius));
            break;
        case 3u:
            linesOverlay.addLines(LinesOverlay::produceSquare(centres[3], outerRadius));
            break;
    }

    linesOverlay.loadVertexes(submissionQueue);
    linesOverlay.recordDraw();
    imageTilesOverlay.loadVertexes(submissionQueue);
    imageTilesOverlay.recordDraw();
}
void Jamblox::loadTextureAtlas(const std::string& filePath)
{
    textureAtlas.initialize(cx::Bitmap::loadFromFile(filePath, cx::Bitmap::Format::RGBA));
}
void Jamblox::loadWorld()
{
    world.initialize(std::make_unique<NaturalWorld>(WORKING_DIR "/data/natural_world/", renderer.retrieve()->bufferBin));
}

void Jamblox::loadTexture(cx::Ref<cx::rhi::vk::SubmissionQueue> submissionQueue)
{
    cx::TgrReadLock textureAtlasLock(textureAtlas, std::defer_lock);
    cx::TgrWriteLock
        viewportLock(viewport, std::defer_lock),
        imageTilesOverlayLock(imageTilesOverlay, std::defer_lock);

    std::lock
        ( textureAtlasLock
        , viewportLock
        , imageTilesOverlayLock
    );

    const cx::Bitmap& bitmap = this->textureAtlas.retrieve(textureAtlasLock);
    viewport.retrieve(viewportLock)->loadTextureAtlas(bitmap, submissionQueue);
    imageTilesOverlay.retrieve(imageTilesOverlayLock)->loadTextureAtlas(bitmap, submissionQueue);
}
void Jamblox::loadDescriptorSets()
{
    {
        cx::TgrWriteLock viewportLock(viewport);
        viewport.retrieve(viewportLock)->loadDescriptorSets();
    }
    {
        cx::TgrWriteLock linesOverlayLock(linesOverlay);
        linesOverlay.retrieve(linesOverlayLock)->loadDescriptorSets();
    }
    {
        cx::TgrWriteLock imageTilesOverlayLock(imageTilesOverlay);
        imageTilesOverlay.retrieve(imageTilesOverlayLock)->loadDescriptorSets();
    }
}
void Jamblox::loadResources()
{
    cx::TgrReadLock rendererLock(renderer);
    std::unique_ptr<cx::rhi::vk::SubmissionQueue> submissionQueue(renderer.retrieve(rendererLock)->createTransferSubmissionQueue());
    rendererLock.unlock();
    loadTexture(submissionQueue);
    loadInventoryOverlayData(submissionQueue);
    loadDescriptorSets();
}
void Jamblox::addWindowEventListeners()
{
    window->addEventListener
        ( cx::io::EventListener<cx::io::glfw::WindowResizedEvent>
            ( [this](const cx::io::glfw::WindowResizedEvent& event)
            {
                const Vector2d cursor = window->cursor();

                window->cursor
                    ( (cursor.x / event.oldSize.x) * event.newSize.x
                    , (cursor.y / event.oldSize.y) * event.newSize.y
                );
            }
        )
    );
    window->addEventListener
        ( cx::io::EventListener<cx::io::glfw::WindowMouseButtonEvent>
            ( [this](const cx::io::glfw::WindowMouseButtonEvent& event)
            {
                if(event.state != cx::io::glfw::ButtonState::RELEASED)
                {
                    cx::decimal radius;
                    switch(event.button)
                    {
                        case cx::io::glfw::MouseButton::LEFT:
                            radius = 4.0_D;
                            break;
                        case cx::io::glfw::MouseButton::RIGHT:
                            radius = 5.0_D;
                            break;
                        default:
                            radius = 0.0_D;
                    }

                    if(radius != 0.0_D)
                    {
                        World::SurfaceVertex* vertex = world.retrieve()->getCameraSightedSurface(player, radius);

                        if(vertex)
                        {
                            if(event.button == cx::io::glfw::MouseButton::LEFT)
                            {
                                changeBlock((vertex->centre - vertex->offset).cast<int>(), Block::NONE);
                            }
                            else if(event.button == cx::io::glfw::MouseButton::RIGHT)
                            {
                                changeBlock((vertex->centre + vertex->offset).cast<int>(), player.inventory.getSelected());
                            }
                        }

                        delete vertex;
                    }
                }
            }
        )
    );
    window->addEventListener
        ( cx::io::EventListener<cx::io::glfw::WindowKeyEvent>
            ( [this](const cx::io::glfw::WindowKeyEvent& event)
            {
                if(!menu)
                {
                    buttons[event.keyCode] = event.state != cx::io::glfw::ButtonState::RELEASED;
                }

                if(event.state == cx::io::glfw::ButtonState::PRESSED && !menu)
                {
                    switch(event.keyCode)
                    {
                        case cx::io::glfw::NUM_1:
                            player.inventory.index = 0u;
                            break;
                        case cx::io::glfw::NUM_2:
                            player.inventory.index = 1u;
                            break;
                        case cx::io::glfw::NUM_3:
                            player.inventory.index = 2u;
                            break;
                        case cx::io::glfw::NUM_4:
                            player.inventory.index = 3u;
                            break;
                        default:
                            break;
                    }
                    std::unique_ptr<cx::rhi::vk::SubmissionQueue> submissionQueue(renderer.retrieve()->createTransferSubmissionQueue());
                    loadInventoryOverlayData(submissionQueue);
                }
                else if(event.state == cx::io::glfw::RELEASED)
                {
                    switch(event.keyCode)
                    {
                        case cx::io::glfw::ESCAPE:
                            toggleMenu();
                            break;
                        case cx::io::glfw::F3:
                            gui.retrieve()->toggleStats();
                            break;
                        default:
                            break;
                    }
                }
            }
        )
    );
    window->addEventListener
        ( cx::io::EventListener<cx::io::glfw::WindowMouseMoveEvent>
            ( [this](const cx::io::glfw::WindowMouseMoveEvent& event)
            {
                if(!menu)
                {
                    cx::decimal
                        size = cx::decimal_cast(window->size().y),
                        sensitivity = settings.rotationSpeed.retrieve().y,
                        midpoint = size/2,
                        amplitude = size/(sensitivity*2.0_D),
                        lower = midpoint - amplitude,
                        upper = midpoint + amplitude;

                    // Clamping cursor y to rotation pitch bounds
                    if(event.newCursor.y < lower)
                    {
                        window->cursor(event.newCursor.x, lower);
                    }
                    else if(event.newCursor.y > upper)
                    {
                        window->cursor(event.newCursor.x, upper);
                    }
                }
            }
        )
    );
}
void Jamblox::toggleMenu()
{
    menu = !menu;

    Gui& gui = *this->gui.retrieve();

    static cx::Vector2d viewportCursor = window->cursor();

    if(menu)
    {
        gui.showMenu();

        viewportCursor = window->cursor();

        const cx::Vector2i& size = window->size();
        window->enableCursor();
        window->cursor(size.cast<double>() / 2.0);
    }
    else
    {
        gui.hideMenu();

        window->cursor(viewportCursor);

        window->disableCursor();
    }
}

std::unordered_set<ChunkLoc> Jamblox::getVisibleChunkLocs()
{
    std::unordered_set<ChunkLoc> chunks;

    int renderDistance = settings.renderDistance.retrieve();

    ChunkLoc centreChunk = Chunk::getContainingChunk(player.position);

    chunks.insert(centreChunk);
    for(int x = 1 - renderDistance; x < renderDistance; ++x)
    {
        for(int y = 1 - renderDistance; y < renderDistance; ++y)
        {
            chunks.insert(centreChunk + ChunkLoc{ x, y });
        }
    }

    return chunks;
}
void Jamblox::updateVisibleChunks()
{
    std::unordered_set<ChunkLoc> newChunkLocs = getVisibleChunkLocs();

    if(currentChunkLocs != newChunkLocs)
    {
        cx::TgrWriteLock
            rendererLock(renderer, std::defer_lock),
            worldLock(world, std::defer_lock);

        std::lock(rendererLock, worldLock);

        std::unique_ptr<Renderer>& renderer = this->renderer.retrieve(rendererLock);
        std::unique_ptr<NaturalWorld>& world = this->world.retrieve(worldLock);

        std::unique_ptr<cx::rhi::vk::SubmissionQueue> submissionQueue(renderer->createTransferSubmissionQueue());

        rendererLock.unlock();

        std::vector<ChunkLoc> chunkLocsToUnload;
        std::copy_if
            (currentChunkLocs.cbegin()
            , currentChunkLocs.cend()
            , std::back_inserter(chunkLocsToUnload)
            , [&newChunkLocs](const ChunkLoc& chunkLoc)
            {
                return newChunkLocs.find(chunkLoc) == newChunkLocs.end();
            }
        );

        for(const ChunkLoc& chunkLoc : chunkLocsToUnload)
        {
            world->removeChunk(chunkLoc);
        }

        std::vector<ChunkLoc> chunkLocsToLoad;
        std::copy_if
            (newChunkLocs.cbegin()
            , newChunkLocs.cend()
            , std::back_inserter(chunkLocsToLoad)
            , [this](const ChunkLoc& chunkLoc)
            {
                return currentChunkLocs.find(chunkLoc) == currentChunkLocs.end();
            }
        );

        // TODO: Parallelize
        for(const ChunkLoc& chunkLoc : chunkLocsToLoad)
        {
            world->addChunk(chunkLoc);
        }

        cx::TgrWriteLock viewportLock(viewport);
        std::shared_ptr<Viewport>& viewport = this->viewport.retrieve(viewportLock);

        for(const ChunkLoc& chunkLoc : chunkLocsToLoad)
        {
            std::shared_ptr<Chunk>& chunk = world->chunks[chunkLoc];

            chunk->computeVertexes();
            chunk->uploadVertexes(viewport, submissionQueue);
        }

        viewport->bindChunks(world->chunks);

        currentChunkLocs = newChunkLocs;

        rendererLock.unlock();
        worldLock.unlock();

        viewport->recordDraw();
    }
}

void Jamblox::changeBlock(const Coordinate& coordinate, Block block)
{
    const ChunkLoc chunkLoc = Chunk::getContainingChunk(coordinate);
    std::shared_ptr<Chunk>& chunk = world.retrieve()->chunks[chunkLoc];

    const Coordinate chunkRelativeCoordinate = coordinate - chunk->origin();

    cx::TgrWriteLock
        viewportLock(viewport, std::defer_lock),
        rendererLock(renderer, std::defer_lock);

    std::lock(viewportLock, rendererLock);

    std::shared_ptr<Viewport>& viewport = this->viewport.retrieve(viewportLock);
    std::unique_ptr<cx::rhi::vk::SubmissionQueue> submissionQueue(renderer.retrieve(rendererLock)->createTransferSubmissionQueue());

    chunk->blocks[Chunk::coordinateToIndex(chunkRelativeCoordinate)] = block;

    chunk->computeVertexes();
    chunk->uploadVertexes(viewport, submissionQueue);

    viewportLock.unlock();
    rendererLock.unlock();

    viewport->recordDraw();
}
