// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

#ifndef JAMBLOX_WORLD_LIGHTING_HH
#define JAMBLOX_WORLD_LIGHTING_HH

#include <corax/maths/vector/vector3.hh>
#include <corax/maths/vector/vector4.hh>

struct Lighting
{
    struct Ambient
    {
        cx::Vector4f color;
    } ambient;
    struct Diffuse
    {
        alignas(16) cx::Vector3f direction;
        alignas(16) cx::Vector4f color;
    } diffuse;
};

#endif // JAMBLOX_WORLD_LIGHTING_HH
