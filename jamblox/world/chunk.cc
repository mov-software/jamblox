// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

#include "chunk.hh"

#include <fstream>
#include <sstream>

Chunk::Chunk(const World& world, ChunkLoc location, BufferBin& bufferBin)
    : blocks(CHUNK_SIZE, Block::NONE)
    , filePath_(world.folderPath + "/chunks/" + std::string(location))
    , location(location)
    , bufferBin(bufferBin)
{}

Chunk::~Chunk()
{
    save();
    bufferBin.add(vertexBuffer_.retrieve());
}

Coordinate Chunk::origin() const
{
    return getChunkOrigin(location);
}
const std::vector<Vertex>& Chunk::vertexes() const
{
    return vertexes_.retrieve();
}
cx::rhi::vk::VertexBuffer& Chunk::vertexBuffer()
{
    return *vertexBuffer_.retrieve();
}

void Chunk::computeVertexes()
{
    const Coordinate origin = this->origin();

    std::vector<Vertex> vertexes(CHUNK_SIZE);

    Coordinate coordinate;

    for(coordinate.y = 0; coordinate.y < CHUNK_HEIGHT; ++coordinate.y)
    {
        for(coordinate.z = 0; coordinate.z < CHUNK_DEPTH; ++coordinate.z)
        {
            for(coordinate.x = 0; coordinate.x < CHUNK_WIDTH; ++coordinate.x)
            {
                int i = Chunk::coordinateToIndex(coordinate);
                vertexes[i] = {origin + coordinate, NO_FACE, this->blocks[i] };
            }
        }
    }

    // Front back face culling
    const cx::Vector3i forward = { 0, 0, 1 };
    for(coordinate.y = 0; coordinate.y < CHUNK_HEIGHT; ++coordinate.y)
    {
        for(coordinate.x = 0; coordinate.x < CHUNK_WIDTH; ++coordinate.x)
        {
            for(coordinate.z = 0; coordinate.z < CHUNK_DEPTH; ++coordinate.z)
            {
                Vertex& vertex = vertexes[Chunk::coordinateToIndex(coordinate)];

                if(vertex)
                {
                    if(!(coordinate.z == 0 ? false : vertexes[Chunk::coordinateToIndex(coordinate - forward)]))
                    {
                        vertex.faces |= FRONT_FACE;
                    }
                    if(!(coordinate.z == CHUNK_DEPTH - 1 ? false : vertexes[Chunk::coordinateToIndex(coordinate + forward)]))
                    {
                        vertex.faces |= BACK_FACE;
                    }
                }
            }
        }
    }

    // Left right face culling
    const cx::Vector3i right = { 1, 0, 0 };
    for(coordinate.y = 0; coordinate.y < CHUNK_HEIGHT; ++coordinate.y)
    {
        for(coordinate.z = 0; coordinate.z < CHUNK_DEPTH; ++coordinate.z)
        {
            for(coordinate.x = 0; coordinate.x < CHUNK_WIDTH; ++coordinate.x)
            {
                Vertex& vertex = vertexes[Chunk::coordinateToIndex(coordinate)];

                if(vertex)
                {
                    if(!(coordinate.x == 0 ? false : vertexes[Chunk::coordinateToIndex(coordinate - right)]))
                    {
                        vertex.faces |= LEFT_FACE;
                    }
                    if(!(coordinate.x == CHUNK_WIDTH - 1 ? false : vertexes[Chunk::coordinateToIndex(coordinate + right)]))
                    {
                        vertex.faces |= RIGHT_FACE;
                    }
                }
            }
        }
    }
    // Top bottom face culling
    const cx::Vector3i up = { 0, 1, 0 };
    for(coordinate.z = 0; coordinate.z < CHUNK_DEPTH; ++coordinate.z)
    {
        for(coordinate.x = 0; coordinate.x < CHUNK_WIDTH; ++coordinate.x)
        {
            for(coordinate.y = 0; coordinate.y < CHUNK_HEIGHT; ++coordinate.y)
            {
                Vertex& vertex = vertexes[Chunk::coordinateToIndex(coordinate)];

                if(vertex)
                {
                    if(!(coordinate.y == 0 ? false : vertexes[Chunk::coordinateToIndex(coordinate - up)]))
                    {
                        vertex.faces |= BOTTOM_FACE;
                    }
                    if(!(coordinate.y == CHUNK_HEIGHT - 1 ? false : vertexes[Chunk::coordinateToIndex(coordinate + up)]))
                    {
                        vertex.faces |= TOP_FACE;
                    }
                }
            }
        }
    }

    // Removing all bricks without rendered faces
    vertexes.erase
        (std::remove_if
            (vertexes.begin()
            , vertexes.end()
            , [](const Vertex& vertex)
            {
                return vertex.faces == 0;
            }
        )
        , vertexes.end()
    );

    vertexes_.initialize(vertexes);
}
void Chunk::uploadVertexes(cx::Ref<Viewport> viewport, cx::Ref<cx::rhi::vk::SubmissionQueue> submissionQueue)
{
    cx::TgrReadLock vertexesLock(vertexes_, std::defer_lock);
    cx::TgrInitializeLock vertexBufferLock(vertexBuffer_, std::defer_lock);

    std::lock(vertexesLock, vertexBufferLock);

    std::shared_ptr<cx::rhi::vk::VertexBuffer>& vertexBuffer = vertexBuffer_.retrieve(vertexBufferLock);

    vertexBuffer.reset
        ( viewport->createVertexBuffer
            ( vertexes_.retrieve(vertexesLock)
            , submissionQueue
        )
    );
}

void Chunk::save()
{
    std::ofstream ofs(filePath_, std::ios::binary | std::ios::out);
    const char* data = reinterpret_cast<const char*>(blocks.data());
    ofs.write(data, static_cast<std::streamsize>(sizeof(Block)/sizeof(char) * blocks.size()));
    ofs.close();
}
void Chunk::load()
{
    std::ifstream ifs(filePath_, std::ios::binary | std::ios::in);

    unsigned int size = CHUNK_SIZE * sizeof(Block);
    char* buffer = new char[size];

    ifs.read(buffer, size);

    Block* blocks = reinterpret_cast<Block*>(buffer);
    this->blocks.assign(blocks, blocks + CHUNK_SIZE);

    ifs.close();

    computeVertexes();
}

Coordinate Chunk::getChunkOrigin(const ChunkLoc& location)
{
    return { location.x * CHUNK_WIDTH, 0, location.y * CHUNK_DEPTH };
}
ChunkLoc Chunk::getContainingChunk(const Coordinate& coordinate)
{
    ChunkLoc chunkLoc
    {
        coordinate.x / CHUNK_WIDTH,
        coordinate.z / CHUNK_DEPTH,
    };

    if(coordinate.x < 0 && coordinate.x % CHUNK_WIDTH != 0)
    {
        --chunkLoc.x;
    }
    if(coordinate.z < 0 && coordinate.z % CHUNK_DEPTH != 0)
    {
        --chunkLoc.y;
    }

    return chunkLoc;
}
ChunkLoc Chunk::getContainingChunk(const cx::Position& position)
{
    return
    {
        static_cast<int>(floor(position.x / CHUNK_WIDTH)),
        static_cast<int>(floor(position.z / CHUNK_DEPTH)),
    };
}
int Chunk::coordinateToIndex(const Coordinate& coordinate)
{
    return  coordinate.y*CHUNK_WIDTH*CHUNK_DEPTH
    +       coordinate.z*CHUNK_WIDTH
    +       coordinate.x;
}
