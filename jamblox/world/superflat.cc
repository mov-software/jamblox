// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

#include "superflat.hh"

#include <numeric>

SuperflatWorld::Chunk::Chunk(const SuperflatWorld& world, ChunkLoc location, BufferBin& bufferBin)
    : ::Chunk
        ( world
        , location
        , bufferBin
    )
{
    for(int y = 0; y < world.height; ++y)
    {
        const Block block = world.getBlockFromHeight(y);

        for(int z = 0; z < CHUNK_DEPTH; ++z)
        {
            for(int x = 0; x < CHUNK_WIDTH; ++x)
            {
                blocks[Chunk::coordinateToIndex({ x, y, z })] = block;
            }
        }
    }
}

SuperflatWorld::SuperflatWorld(std::string folderPath, BufferBin& bufferBin)
    : World(std::move(folderPath), bufferBin)
    , layers
    (
        {
            { Block::BEDROCK, 1 },
            { Block::STONE, 10 },
            { Block::DIRT, 3 },
            { Block::SAND, 1 },
        }
    )
    , height
        ( std::accumulate
            ( layers.cbegin()
            , layers.cend()
            , 0
            , [](int y, const Layer& layer)
            {
                return y + layer.thickness;
            }
        )
    )
{}

Block SuperflatWorld::getBlockFromHeight(int height) const
{
    for(const Layer& layer : layers)
    {
        height -= layer.thickness;
        if(height < 0)
        {
            return layer.block;
        }
    }

    return Block::NONE;
}

void SuperflatWorld::createChunk(ChunkLoc location)
{
    World::createChunk(std::make_shared<SuperflatWorld::Chunk>(*this, location, bufferBin));
}
