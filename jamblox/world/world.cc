// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

#include "world.hh"

#include <jamblox/world/chunk.hh>

#include <unordered_set>
#include <fstream>

World::World(std::string folderPath, BufferBin& bufferBin)
    : folderPath(std::move(folderPath))
    , bufferBin(bufferBin)
{}

void World::addChunk(const ChunkLoc& location)
{
    std::ifstream file(folderPath + "/chunks/" + std::string(location));

    if(file.good())
    {
        loadChunk(location);
    }
    else
    {
        createChunk(location);
    }
}
void World::removeChunk(const ChunkLoc& location)
{
    chunks.erase(location);
}

void World::createChunk(std::shared_ptr<Chunk>&& chunk)
{
    chunks[chunk->location] = std::move(chunk);
}
void World::loadChunk(ChunkLoc location)
{
    std::shared_ptr<Chunk> chunk = std::make_shared<Chunk>(*this, location, bufferBin);
    chunk->load();
    chunks[location] = std::move(chunk);
}

World::SurfaceVertex::SurfaceVertex(cx::Position centre, cx::Vector3D normal, cx::Vector3b bounds, const Camera& camera)
    : centre(centre)
    , normal(normal)
    , offset(0.5_D * normal)
    , lambda(dot(normal, (centre - camera.position))/dot(normal, camera.axes().forward))
    , intersection(camera.position + lambda*camera.axes().forward)
    , intersects(true)
{
    if(lambda < 0)
    {
        intersects = false;
    }
    else
    {
        if(bounds.x)
        {
            const cx::decimal bound = 0.5_D * cx::decimal_cast(bounds.x);
            if(intersection.x < centre.x - bound || intersection.x > centre.x + bound)
            {
                intersects = false;
            }
        }
        if(bounds.y)
        {
            const cx::decimal bound = 0.5_D * cx::decimal_cast(bounds.y);
            if(intersection.y < centre.y - bound || intersection.y > centre.y + bound)
            {
                intersects = false;
            }
        }
        if(bounds.z)
        {
            const cx::decimal bound = 0.5_D * cx::decimal_cast(bounds.z);
            if(intersection.z < centre.z - bound || intersection.z > centre.z + bound)
            {
                intersects = false;
            }
        }
    }
}
World::SurfaceVertex* World::getCameraSightedSurface(const Camera& camera, cx::decimal radius)
{
    const cx::Vector3D
        forward(0, 0, 1),
        up(0, 1, 0),
        right(1, 0, 0);

    std::unordered_set<ChunkLoc> containingChunkLocs;
    {
        const cx::Vector3D
            f = radius * forward,
            r = radius * right;

        // Radius boundaries
        const std::vector<cx::Position> bounds
        {
            camera.position + f,
            camera.position + r,
            camera.position - f,
            camera.position - r,
        };

        std::transform
            ( bounds.cbegin()
            , bounds.cend()
            , std::inserter(containingChunkLocs, containingChunkLocs.end())
            , [](const cx::Position& bound)
            {
                return Chunk::getContainingChunk(bound);
            }
        );
    }

    std::vector<SurfaceVertex> surfaces;

    for(const ChunkLoc& chunkLoc : containingChunkLocs)
    {
        const std::shared_ptr<Chunk>& chunk = chunks[chunkLoc];
        const std::vector<::Vertex>& vertexes = chunk->vertexes();

        for(const ::Vertex& vertex : vertexes)
        {
            const cx::Position origin = vertex.position.cast<cx::decimal>();

            const std::vector<cx::Vector3D> directions = vertex.getVisibleNormals();

            for(const cx::Vector3D& direction : directions)
            {
                if (dot(direction, camera.axes().forward) < 0)
                {
                    cx::Vector3b bounds = direction.transform<bool>([](cx::decimal x)
                    {
                        return !static_cast<bool>(x);
                    });

                    const cx::Position centre = origin + 0.5f*direction;
                    cx::decimal distance = (camera.position - centre).magnitude();

                    if(distance <= radius)
                    {
                        surfaces.emplace_back
                            ( centre
                            , direction
                            , bounds
                            , camera
                        );
                    }
                }
            }
        }
    }

    surfaces.erase
        ( std::remove_if
            ( surfaces.begin()
            , surfaces.end()
            , [](const SurfaceVertex& surface)
            {
                return !surface.intersects;
            }
        ), surfaces.end()
    );

    std::vector<SurfaceVertex>::iterator surface = std::min_element
        ( surfaces.begin()
        , surfaces.end()
        , [](const SurfaceVertex& a, const SurfaceVertex& b)
        {
            return a.lambda < b.lambda;
        }
    );

    if(surface != surfaces.end())
    {
        return new SurfaceVertex(*surface);
    }
    else
    {
        return nullptr;
    }
}
