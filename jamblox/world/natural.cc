// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

#include <utility>

#include "natural.hh"

NaturalWorld::Layer::Layer(const NaturalWorld& world, Block block, cx::decimal frequency, cx::decimal amplitude, int offset, int octaves)
    : block(block)
    , frequency(frequency)
    , amplitude(amplitude)
    , offset(offset)
    , octaves(octaves)
    , perlin(world.seed + static_cast<int>(block))
{}

int NaturalWorld::Layer::height(const cx::Vector2D& coordinate) const
{
    return offset + static_cast<int>
        ( round
            (
                ( amplitude
                * perlin.octave2D_01
                    ( coordinate.x / frequency
                    , coordinate.y / frequency
                    , octaves
                )
            )
        )
    );
}

NaturalWorld::Chunk::Chunk(const NaturalWorld& world, ChunkLoc location, BufferBin& bufferBin)
    : ::Chunk
        ( world
        , location
        , bufferBin
    )
{
    for(int x = 0; x < CHUNK_WIDTH; ++x)
    {
        for(int z = 0; z < CHUNK_DEPTH; ++z)
        {
            int y = 0;

            const Coordinate origin = Chunk::getChunkOrigin(location);
            const cx::Vector2D coordinate(cx::decimal_cast(origin.x + x), cx::decimal_cast(origin.z + z));

            for(const Layer& layer : world.layers)
            {
                int height = y + layer.height(coordinate);

                for(; y < height; ++y)
                {
                    blocks[Chunk::coordinateToIndex({ x, y, z })] = layer.block;
                }
            }
        }
    }
}

NaturalWorld::NaturalWorld(std::string folderPath, BufferBin& bufferBin)
    : World(std::move(folderPath), bufferBin)
    , seed(123456)
    , layers
    (
        {
            { *this, Block::BEDROCK, 0.0, 0.0, 1 },
            { *this, Block::STONE, 32.0, 64.0, 8, 5 },
            { *this, Block::DIRT, 8.0, 1.0, 0, 5 },
            { *this, Block::SAND, 8.0, 1.0, 0, 5 },
        }
    )
{}

void NaturalWorld::createChunk(ChunkLoc location)
{
    World::createChunk(std::make_shared<NaturalWorld::Chunk>(*this, location, bufferBin));
}
