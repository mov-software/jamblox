// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

#include "vertex.hh"

Vertex::Vertex(const cx::Vector3i& position, Faces faces, Block block)
    : position(position)
    , faces(faces)
    , block(block)
{}

Vertex::operator bool() const
{
    return block != Block::NONE;
}

std::vector<cx::Vector3D> Vertex::getVisibleNormals() const
{
    std::vector<cx::Vector3D> normals;

    if(faces & FRONT_FACE)
    {
        normals.emplace_back(0, 0, -1);
    }
    if(faces & TOP_FACE)
    {
        normals.emplace_back(0, 1, 0);
    }
    if(faces & RIGHT_FACE)
    {
        normals.emplace_back(1, 0, 0);
    }
    if(faces & BACK_FACE)
    {
        normals.emplace_back(0, 0, 1);
    }
    if(faces & BOTTOM_FACE)
    {
        normals.emplace_back(0, -1, 0);
    }
    if(faces & LEFT_FACE)
    {
        normals.emplace_back(-1, 0, 0);
    }

    return normals;
}
