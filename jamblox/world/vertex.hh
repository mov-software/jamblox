// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

#ifndef JAMBLOX_WORLD_VERTEX_HH
#define JAMBLOX_WORLD_VERTEX_HH

#include <jamblox/world/block.hh>

#include <corax/memory/bit.hh>
#include <corax/general/flags.hh>

enum FaceBits : cx::Flags
{
    NO_FACE     = 0,
    FRONT_FACE  = CX_BIT(0),
    TOP_FACE    = CX_BIT(1),
    RIGHT_FACE  = CX_BIT(2),
    BACK_FACE   = CX_BIT(3),
    BOTTOM_FACE = CX_BIT(4),
    LEFT_FACE   = CX_BIT(5),
};
typedef cx::FlagBits Faces;

struct Vertex
{
    cx::Vector3i position;
    Faces faces;
    Block block;

    Vertex
        ( const cx::Vector3i& position = { 0, 0, 0 }
        , Faces faces = NO_FACE
        , Block block = Block::NONE
    );

    operator bool() const;

    std::vector<cx::Vector3D> getVisibleNormals() const;
};

#endif // JAMBLOX_WORLD_VERTEX_HH
