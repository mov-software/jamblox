// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

#ifndef JAMBLOX_WORLD_SUPERFLAT_HH
#define JAMBLOX_WORLD_SUPERFLAT_HH

#include <jamblox/world/chunk.hh>

class SuperflatWorld : public World
{
public:
    struct Layer
    {
        Block block;
        int thickness;
    };
    class Chunk : public ::Chunk
    {
    public:
        Chunk(const SuperflatWorld& world, ChunkLoc location, BufferBin& bufferBin);
    };

    const std::vector<Layer> layers;
    const unsigned int height;

    SuperflatWorld(std::string folderPath, BufferBin& bufferBin);

    Block getBlockFromHeight(int height) const;

    void createChunk(ChunkLoc location) override;
};

#endif // JAMBLOX_WORLD_SUPERFLAT_HH
