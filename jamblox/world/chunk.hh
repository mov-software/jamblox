// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

#ifndef JAMBLOX_WORLD_CHUNK_HH
#define JAMBLOX_WORLD_CHUNK_HH

#include <jamblox/graphics/layers/viewport.hh>

#include <jamblox/config.h>

#include <jamblox/world/world.hh>
#include <jamblox/world/block.hh>
#include <jamblox/world/space.hh>

#include <corax/graphics/rhi/vk/buffer.hh>

class Chunk
{
public:
    std::vector<Block> blocks;
private:
    cx::Tgr<std::vector<Vertex>> vertexes_;
    cx::Tgr<std::shared_ptr<cx::rhi::vk::VertexBuffer>> vertexBuffer_;

    BufferBin& bufferBin;

    const std::string filePath_;
public:
    const ChunkLoc location;

    Chunk(const World& world, ChunkLoc location, BufferBin& bufferBin);

    ~Chunk();

    Coordinate origin() const;
    const std::vector<Vertex>& vertexes() const;
    cx::rhi::vk::VertexBuffer& vertexBuffer();

    void computeVertexes();
    void uploadVertexes(cx::Ref<Viewport> viewport, cx::Ref<cx::rhi::vk::SubmissionQueue> submissionQueue);

    void save();
    void load();

    static Coordinate getChunkOrigin(const ChunkLoc& location);
    static ChunkLoc getContainingChunk(const Coordinate& coordinate);
    static ChunkLoc getContainingChunk(const cx::Position& position);
    static int coordinateToIndex(const Coordinate& coordinate);
};

#endif // JAMBLOX_WORLD_CHUNK_HH
