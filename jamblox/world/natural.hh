// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

#ifndef JAMBLOX_WORLD_NATURAL_HH
#define JAMBLOX_WORLD_NATURAL_HH

#include <jamblox/world/chunk.hh>

#include <PerlinNoise/PerlinNoise.hpp>

class NaturalWorld : public World
{
public:
    struct Layer
    {
        Block block;
        cx::decimal frequency, amplitude;
        int offset, octaves;

        const siv::PerlinNoise perlin;

        Layer(const NaturalWorld& world, Block block = Block::NONE, cx::decimal frequency = 1.0, cx::decimal amplitude = 0.0, int offset = 0, int octaves = 0);

        int height(const cx::Vector2D& coordinate) const;
    };
    class Chunk : public ::Chunk
    {
    public:
        Chunk(const NaturalWorld& world, ChunkLoc location, BufferBin& bufferBin);
    };

    const siv::PerlinNoise::seed_type seed;

    const std::vector<Layer> layers;

    NaturalWorld(std::string folderPath, BufferBin& bufferBin);

    void createChunk(ChunkLoc location) override;
};

#endif // JAMBLOX_WORLD_NATURAL_HH
