// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

#ifndef JAMBLOX_WORLD_HH
#define JAMBLOX_WORLD_HH

#include <jamblox/world/vertex.hh>
#include <jamblox/world/space.hh>

#include <jamblox/graphics/bufferBin.hh>

#include <corax/core/tgr.hh>

#include <unordered_map>

class Chunk;
class Camera;

class World
{
public:
    const std::string folderPath;

    std::unordered_map<ChunkLoc, std::shared_ptr<Chunk>> chunks;

    BufferBin& bufferBin;

    World(std::string folderPath, BufferBin& bufferBin);

    void addChunk(const ChunkLoc& location);
    void removeChunk(const ChunkLoc& location);

    void createChunk(std::shared_ptr<Chunk>&& chunk);
    virtual void createChunk(ChunkLoc location) = 0;
    void loadChunk(ChunkLoc location);

    struct SurfaceVertex
    {
        cx::Position centre;
        cx::Vector3D normal;
        cx::Vector3D offset;

        cx::decimal lambda;
        cx::Position intersection;
        bool intersects;

        SurfaceVertex(cx::Position origin, cx::Vector3D n, cx::Vector3b bounds, const Camera& camera);
    };
    SurfaceVertex* getCameraSightedSurface(const Camera& camera, cx::decimal radius);
};

#endif // JAMBLOX_WORLD_HH
